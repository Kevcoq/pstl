package xml.enumeration;

/**
 * Enum du status pour info [ ok ; ko ]
 * 
 * @author Kevcoq
 * 
 */
public enum InfoStatus {
	ok, ko
}
