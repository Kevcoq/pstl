package xml.enumeration;

/**
 * Enum des types pour pcf dans le xml [ request ; answer ; advise ]
 * 
 * @author kevcoq
 * 
 */
public enum PcfType {
	request, answer, advise
}
