package xml;

import socket.InitException;
import circuit.exception.BadCircuit;

/**
 * Interface des services xml
 * 
 * @author kevcoq
 * 
 */
public interface IXML {
	/**
	 * protocole scenario
	 * 
	 * @return un xml pour le scenario
	 */
	public String getScenarioXML();

	/**
	 * protocole topography
	 * 
	 * @return un xml pour la topo
	 */
	public String getTopoXML();

	/**
	 * protocole init
	 * 
	 * @return un xml pour l' init
	 * @throws BadCircuit
	 *             exception si le circuit a une incoherence
	 */
	public String getInitXML() throws BadCircuit;

	/**
	 * protocole set
	 * 
	 * @return un xml pour le set
	 * @throws InitException 
	 */
	public void getSetXML() throws InitException;

	/**
	 * protocole lights
	 * 
	 * @return un xml pour les leds
	 */
	public String getLightsXML();
}
