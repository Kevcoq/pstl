package xml;

import socket.GestionSocket;
import socket.InitException;
import socket.echange.Write;
import xml.enumeration.PcfType;
import circuit.exception.BadCircuit;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.ILed;
import circuit.inter.IRame;

/**
 * Genere les messages xml adaptes en fonction du circuit
 * 
 * @author Kevcoq
 * 
 */
public class CircuitToXML implements IXML {
	ICircuit circuit;

	/**
	 * Constructeur
	 * 
	 * @param circuit
	 *            le circuir
	 */
	public CircuitToXML(ICircuit circuit) {
		super();
		this.circuit = circuit;
	}

	@Override
	public String getScenarioXML() {
		return "<scenario id=\"" + circuit.getScenario() + "\"/>";
	}

	@Override
	public String getTopoXML() {
		String s = "";
		s += "<topography>";

		for (IEltCircuit elt : circuit.getList()) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				ICapteur capt = (ICapteur) elt;
				s += "<edges>";

				s += capt.getXML();

				s += "<in>";
				// si il y a un precedant
				if (capt.prec() != null) {
					for (ICapteur tmp : capt.prec())
						s += tmp.getXML();
				}
				s += "</in>";

				s += "<out>";
				// si il y a un suivant
				if (capt.suiv() != null) {
					for (ICapteur tmp : capt.suiv())
						s += tmp.getXML();
				}
				s += "</out>";

				s += "</edges>";
			}
		}
		s += "</topography>";
		return s;
	}

	@Override
	public String getInitXML() throws BadCircuit {
		String s = "";
		s += "<init>";

		// on prend le dernier
		IEltCircuit before = circuit.getList()
				.get(circuit.getList().size() - 1);

		for (IEltCircuit elt : circuit.getList()) {
			// si c'est un train
			if (elt instanceof IRame) {
				// si before est null ou bien que ce n'est pas un capteur =>
				// probleme
				if (before == null || !(before instanceof ICapteur)
						|| ((ICapteur) before).suiv() == null) {
					throw new BadCircuit("le train n'est pas entre 2 station");
				}

				IRame train = (IRame) elt;
				s += "<position>";
				s += "<before>";
				s += before.getXML();
				s += "</before>";

				s += train.getXML();
				s += "<after>";
				for (ICapteur tmp : ((ICapteur) before).suiv())
					s += tmp.getXML();
				s += "</after>";

				s += "</position>";
			} else if (elt instanceof ICapteur) {
				// si c'est un capteur, on remplace before
				before = (ICapteur) elt;
			} else {
				// sinon, on ne sait pas ce que sait => exception
				throw new BadCircuit("elt non train ni capteur");
			}
		}
		s += "</init>";
		return s;
	}

	@Override
	public void getSetXML() throws InitException {
		for (IEltCircuit elt : circuit.getList()) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				ILed led = ((ICapteur) elt).getFeu();
				if (led != null && led.changement()) {
					Write.writePCF(GestionSocket.getReqId(), PcfType.request,
							"<set>" + led.getXML() + "</set>");
					led.resetChangement();
				}
			} else if (elt instanceof IRame && ((IRame) elt).changement()) {
				Write.writePCF(GestionSocket.getReqId(), PcfType.request,
						"<set>" + elt.getXML() + "</set>");
				((IRame) elt).resetChangement();
			}
		}
	}

	@Override
	public String getLightsXML() {
		String s = "";
		s += "<lights>";

		for (IEltCircuit elt : circuit.getList()) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				ILed led = ((ICapteur) elt).getFeu();
				if (led != null) {
					s += led.getXML();
				}
			}
		}
		s += "</lights>";
		return s;
	}
}