package xml;

import xml.enumeration.InfoStatus;
import xml.enumeration.PcfType;

/**
 * Definit les messages xml PCF ne dependant pas du circuit
 * 
 * @author Kevcoq
 * 
 */
public abstract class PCF implements IXML {

	/**
	 * dtd
	 * 
	 * @param corps
	 *            le corps du xml
	 * @return un xml avec la dtd
	 */
	public static String getDTD(String corps) {
		String s = "<!DOCTYPE pcf SYSTEM \"pcf.dtd\">";
		s += corps;
		return s;
	}

	/**
	 * protocole pcf
	 * 
	 * @param id
	 *            l'identifiant reqid
	 * @param type
	 *            le type du pcf
	 * @param corps
	 *            le corps du xml
	 * @return un xml pcf avec comme contenu corps
	 */
	public static String getPCF(String id, PcfType type, String corps) {
		String s = "";
		s += "<pcf reqid=\"" + id + "\" type=\"" + type + "\">";
		s += corps;
		s += "</pcf>";
		return s;
	}

	/**
	 * protocole start
	 * 
	 * @return un xml pour le start
	 */
	public static String getStartXML() {
		return "<start/>";
	}

	/**
	 * protocole hello
	 * 
	 * @param idHello
	 *            l'id du hello
	 * @return un xml hello
	 */
	public static String getHelloXML(String idHello) {
		return "<hello id=\"" + idHello + "\"/>";
	}

	/**
	 * protocole olleh
	 * 
	 * @param idOlleh
	 *            l'identifiant de Hello
	 * @return un xml olleh
	 */
	public static String getOllehXML(String id, String idOlleh, PcfType type) {
		if (idOlleh != null) {
			return "\t<olleh id=\"" + idOlleh + "\"/>";
		} else {
			return "\t<olleh/>";
		}
	}

	/**
	 * protocole info
	 * 
	 * @param txt
	 *            le message
	 * @param status
	 *            le status (ok|ko)
	 * @return un xml info
	 */
	public static String getInfoXML(String txt, InfoStatus status) {
		String s = "";
		s += "<info status=\"" + status + "\">";
		s += txt;
		s += "</info>";
		return s;
	}

	/**
	 * protocole bye
	 * 
	 * @return un xml bye
	 */
	public static String getByeXML() {
		return "<bye/>\n";
	}
}
