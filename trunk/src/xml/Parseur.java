package xml;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import circuit.FactoryCircuit;
import circuit.exception.BadCircuit;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;

/**
 * Le parseur, utilisation de SAX
 * 
 * Utilisation de si-sinon au lieu de switch -> Java 1.6
 * 
 * @author kevcoq
 * 
 */
public class Parseur extends DefaultHandler {
	// attribut
	private ICircuit circuit;

	// annexe
	// private String pcfReqId;
	// private PcfType pcfType;
	private int edgInOut = 0; // 0 rien, 1 ajout, 2 in, 3 out, 4 up
	private List<String> in = null;
	private List<String> out = null;
	private String svid;
	private boolean svStation;

	/**
	 * constructeur
	 * 
	 * @param circuit
	 *            le circuit
	 */
	public Parseur(ICircuit circuit) {
		super();
		this.circuit = circuit;
	}

	/**
	 * Ouverture de la balise
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		try {
			if (qName.equals("pcf")) {
				// pcfReqId = attributes.getValue("reqid");
				// String pcf_tmp = attributes.getValue("type");
				// if (pcf_tmp.equals("request")) {
				// pcfType = PcfType.request;
				// } else if (pcf_tmp.equals("answer")) {
				// pcfType = PcfType.answer;
				// } else if (pcf_tmp.equals("advise")) {
				// pcfType = PcfType.advise;
				// }
			} else if (qName.equals("scenario")) {
				try {
					String id_scenario = attributes.getValue("id");
					if (id_scenario == null) {
						id_scenario = "0";
					}
					circuit.setScenario(Integer.parseInt(id_scenario));
				} catch (Exception e) {
					// erreur, le contenu de id n'est pas un entier
					throw new SAXException(e);
				}
			} else if (qName.equals("edges")) {
				edgInOut = 1; // ajout
				in = new ArrayList<String>();
				out = new ArrayList<String>();
			} else if (qName.equals("capteur")) {
				int id_capteur = Integer.parseInt(attributes.getValue("id"));
				String b_tmp = null;
				boolean estStation = false;
				if ((b_tmp = attributes.getValue("type")) != null
						&& b_tmp.equals("station")) {
					estStation = true;
				}
				switch (edgInOut) {
				case 1: // ajout
					svid = "c" + id_capteur;
					svStation = estStation;
					break;
				case 2: // in
					ICapteur tmpI = FactoryCircuit.createCapteur("c"
							+ id_capteur, circuit, estStation);
					in.add(tmpI.getId());
					circuit.put(tmpI);
					break;
				case 3: // out
					ICapteur tmpO = FactoryCircuit.createCapteur("c"
							+ id_capteur, circuit, estStation);
					out.add(tmpO.getId());
					circuit.put(tmpO);
					break;

				case 4: // up
					((ICapteur) circuit.recherche_id("c" + id_capteur)).on();
					System.out.println("up -> capt : "
							+ circuit.recherche_id("c" + id_capteur).getId());
					break;

				default:
					break;
				}
			} else if (qName.equals("in")) {
				edgInOut = 2; // in
			} else if (qName.equals("out")) {
				edgInOut = 3; // out
			} else if (qName.equals("position")) {
				in = new ArrayList<String>();
				out = new ArrayList<String>();
			} else if (qName.equals("before")) {
				edgInOut = 2; // in
			} else if (qName.equals("train")) {
				int id_train = Integer.parseInt(attributes.getValue("id"));
				String action_train = attributes.getValue("action"), dir_train = attributes
						.getValue("dir");

				if (dir_train == null)
					dir_train = "forward";

				boolean roule = false;
				if (action_train != null && action_train.equals("start"))
					roule = true;

				circuit.ajouterApres((FactoryCircuit.createTrain(
						"t" + id_train, circuit, dir_train, roule)), in.get(0));
			} else if (qName.equals("light")) {
				int id_capt = Integer.parseInt(attributes.getValue("id"));
				String color_led = attributes.getValue("color");

				if (color_led == null)
					color_led = "red";

				FactoryCircuit.createFeu("c" + id_capt, circuit, color_led);
			} else if (qName.equals("up")) {
				edgInOut = 4; // up
			} else if (qName.equals("hello")) {
			} else if (qName.equals("olleh")) {
			} else if (qName.equals("topography")) {
			} else if (qName.equals("init")) {
			} else if (qName.equals("after")) {
			} else if (qName.equals("lights")) {
			} else if (qName.equals("set")) {
			} else if (qName.equals("info")) {
			} else if (qName.equals("bye")) {
			} else {
				// erreur, on peut lever une exception
				throw new SAXException("Balise " + qName + " inconnue.");
			}
		} catch (ID_Error e) {
			e.printStackTrace();
		}
	}

	/**
	 * detection fin de balise
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		try {
			if (qName.equals("edges")) {
				circuit.ajouter(FactoryCircuit.createCapteur(svid, circuit,
						svStation, in, out));
				edgInOut = 0; // defaut
			} else if (qName.equals("in")) {
				edgInOut = 0; // defaut

			} else if (qName.equals("out")) {
				edgInOut = 0; // defaut
			} else if (qName.equals("before")) {
				edgInOut = 0; // defaut
			} else if (qName.equals("lights")) {
			} else if (qName.equals("up")) {
				edgInOut = 0; // defaut
			} else if (qName.equals("pcf")) {
			} else if (qName.equals("hello")) {
			} else if (qName.equals("olleh")) {
			} else if (qName.equals("scenario")) {
			} else if (qName.equals("topography")) {
			} else if (qName.equals("init")) {
			} else if (qName.equals("position")) {
			} else if (qName.equals("capteur")) {
			} else if (qName.equals("train")) {
			} else if (qName.equals("after")) {
			} else if (qName.equals("light")) {
			} else if (qName.equals("set")) {
			} else if (qName.equals("info")) {
			} else if (qName.equals("bye")) {
			} else {
				// erreur, on peut lever une exception
				throw new SAXException("Balise " + qName + " inconnue.");
			}
		} catch (ID_Error e) {
			e.printStackTrace();
		} catch (BadCircuit e) {
			e.printStackTrace();
		}
	}

	/**
	 * debut du parsing
	 */
	public void startDocument() throws SAXException {
	}

	/**
	 * fin du parsing
	 */
	public void endDocument() throws SAXException {
	}

	/**
	 * Renvoie le clone du circuit
	 * 
	 * @return le clone du circuit
	 */
	public ICircuit getCircuitClone() {
		return circuit.clone();
	}
}
