package algo;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import socket.GestionSocket;
import socket.InitException;
import socket.echange.LectureThread;
import socket.echange.Write;
import xml.CircuitToXML;
import xml.IXML;
import xml.PCF;
import xml.Parseur;
import xml.enumeration.PcfType;
import algo.strategy.FactoryScenario;
import algo.strategy.IScenario;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICircuit;

/**
 * Thread d'analyse du xml recu et de traitement
 * 
 * @author Kevcoq
 * 
 */
public class Analyse extends Thread {
	private ICircuit circuit;
	private LectureThread lecture;
	private IXML xmlCircuit;

	// SAX
	private DefaultHandler gestionnaire;
	private SAXParser parseur;

	/**
	 * Constructeur
	 * 
	 * @param circuit
	 *            le circuit
	 * @param lecture
	 *            le thread de lecture
	 */
	public Analyse(ICircuit circuit, LectureThread lecture) {
		super();
		try {
			this.circuit = circuit;
			this.lecture = lecture;
			this.xmlCircuit = new CircuitToXML(circuit);

			// SAX
			this.gestionnaire = new Parseur(circuit);
			SAXParserFactory fact = SAXParserFactory.newInstance();
			this.parseur = fact.newSAXParser();

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			IScenario scenario = null;
			String rep;

			// on attend de recevoir la 1 er reponse
			while ((rep = lecture.getRep()) == null) {
				sleep(10);
			}

			// puis on boucle
			while (true) {
				if (rep != null) {
					parseur.parse(new InputSource(new StringReader(rep)),
							gestionnaire);
				} else {
					sleep(100);
				}
				if (scenario == null) {
					scenario = FactoryScenario.creerScenario(circuit);
					scenario.start();
					Write.writePCF(GestionSocket.getReqId(), PcfType.request,
							PCF.getStartXML());

					xmlCircuit.getSetXML();

					// check circuit
					if (!CheckIntegrityCircuit.checkIntegrityCircuit(circuit)) {
						System.out.println("error check circuit");
						return;
					}

					circuit.reprendre();
				}
				scenario.execute();
				if (circuit.changement()) {
					xmlCircuit.getSetXML();
				}
				rep = lecture.getRep();
			}
		} catch (BadCircuit e) {
			e.printStackTrace();
		} catch (ElementNotFoundException e) {
			e.printStackTrace();
		} catch (ID_Error e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InitException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * renvoie le clone du circuit
	 * 
	 * @return le clone du circuit
	 */
	public ICircuit getCircuitClone() {
		return ((Parseur) gestionnaire).getCircuitClone();
	}

}
