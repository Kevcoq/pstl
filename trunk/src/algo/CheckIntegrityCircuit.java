package algo;

import java.util.List;

import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.enumeration.TrainDirection;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.ILed;
import circuit.inter.IRame;

/**
 * Verifie l'integrite du circuit
 * 
 * @author Kevcoq
 * 
 */
public class CheckIntegrityCircuit {
	public static boolean checkIntegrityCircuit(ICircuit circuit) {
		try {
			checkScenario(circuit.getScenario());
			// scenario 4, pas de feu pour le capteur 0
			if (circuit.getScenario() != 4 && circuit.getScenario() != 3)
				checkListElt(circuit.getList());
			checkCircuit(circuit);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static void checkCircuit(ICircuit circuit) throws Exception {
		for (IEltCircuit elt : circuit.getList()) {
			if (elt instanceof IRame
					&& !(circuit.prec(elt) instanceof ICapteur)
					&& !(circuit.suiv(elt) instanceof ICapteur)) {
				throw new Exception("checkCircuit\n");
			}
		}
	}

	private static void checkListElt(List<IEltCircuit> list) throws Exception {
		for (IEltCircuit elt : list) {
			if (elt instanceof IRame) {
				checkTrain((IRame) elt);
			} else if (elt instanceof ICapteur) {
				checkCapteur((ICapteur) elt);
			}
		}
	}

	private static void checkCapteur(ICapteur capt) throws Exception {
		if (capt.getFeu() != null)
			checkFeu(capt.getFeu());

		if (capt.suiv() != null && capt.feuSuivant() == null)
			throw new Exception("checkCapteurFeu\n");

		if (capt.prec() != null && capt.feuPrec() == null)
			throw new Exception("checkCapteurFeu\n");
	}

	private static void checkFeu(ILed feu) throws Exception {
		if (feu.getCouleur() != FeuCouleur.green
				&& feu.getCouleur() != FeuCouleur.red) {
			throw new Exception("checkFeu\n");
		}
	}

	private static void checkTrain(IRame train) throws Exception {
		if (train.getDirection() != TrainDirection.forward
				&& train.getDirection() != TrainDirection.backward
				&& train.getEtat() != TrainAction.start
				&& train.getEtat() != TrainAction.stop)
			throw new Exception("checkTrain\n");
	}

	private static void checkScenario(int scenario) throws Exception {
		if (scenario > 4 && scenario < 0) {
			throw new Exception("checkScenario\n");
		}
	}
}
