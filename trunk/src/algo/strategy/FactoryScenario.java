package algo.strategy;

import circuit.exception.BadCircuit;
import circuit.inter.ICircuit;

/**
 * Creer les scenarios
 * 
 * @author Kevcoq
 * 
 */
public class FactoryScenario {
	/**
	 * creer un scenario
	 * 
	 * @param circuit
	 *            le circuit
	 * @return un scenario
	 */
	public static IScenario creerScenario(ICircuit circuit) {
		switch (circuit.getScenario()) {
		case 0:
			return new Scenario0(circuit);
		case 1:
			return new Scenario1(circuit);
		case 2:
			return new Scenario2(circuit);
		case 3:
			return new Scenario3(circuit);
		case 4:
			return new Scenario4(circuit);
		default:
			try {
				throw new BadCircuit("scenario " + circuit.getScenario()
						+ " inexistant\n");

			} catch (BadCircuit e) {
				System.err.println("Scenarion non reconnu.");
				e.printStackTrace();
			}
		}
		return null;
	}
}
