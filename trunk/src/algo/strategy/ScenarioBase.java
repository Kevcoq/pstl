package algo.strategy;

import java.util.List;
import java.util.Random;

import circuit.enumeration.FeuCouleur;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Base des scenarios
 * @author Kevcoq
 *
 */
public abstract class ScenarioBase implements IScenario {

	protected ICircuit circuit;
	protected Random rand;

	protected ScenarioBase(ICircuit circuit) {
		super();
		this.circuit = circuit;
		this.rand = new Random();
	}

	@Override
	public void start() throws BadCircuit, ElementNotFoundException {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				if (circuit.suiv(elt) instanceof ICapteur) {
					((ICapteur) elt).getFeu().setCouleur(FeuCouleur.green);
				}
			}
		}

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un train
			if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				ICapteur capt = (ICapteur) circuit.suiv(train);

				if (capt.getFeu().getCouleur() == FeuCouleur.green) {
					// si le feu suivant est vert
					train.start();
				}
			}
		}
	}
}
