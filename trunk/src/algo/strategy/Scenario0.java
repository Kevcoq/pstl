package algo.strategy;

import java.util.List;

import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Scenario 0, boucle de troncon
 * 
 * @throws BadCircuit
 * @throws ElementNotFoundException
 * @throws ID_Error
 */
public class Scenario0 extends ScenarioBase {

	public Scenario0(ICircuit circuit) {
		super(circuit);
	}

	@Override
	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			// si c'est un capteur actif
			if (elt instanceof ICapteur && ((ICapteur) elt).getStatut()) {
				ICapteur capt = (ICapteur) elt;
				// on recupere la rame qui a declenche le capteur
				IRame train = (IRame) circuit.prec(capt);

				// on passe le feu au rouge
				capt.getFeu().setCouleur(FeuCouleur.red);

				// on avance le train et le stop
				circuit.avancerTrain(train, capt);
				train.stop();

				// le feu prec au vert
				capt.feuPrec().get(0).setCouleur(FeuCouleur.green);

				if ((capt.feuSuivant()).get(0).getCouleur() == FeuCouleur.green) {
					// si le feu d'apres est vert, on continue
					train.start();
				}

				// capteur traite -> off
				capt.off();
			} else if (elt instanceof IRame) {
				IRame train = (IRame) elt;

				// si c'est un train a l'arret
				if (train.getEtat() == TrainAction.stop) {
					ICapteur capt = (ICapteur) circuit.suiv(train);
					if (capt.getFeu().getCouleur() == FeuCouleur.green) {
						// si le feu suivant est vert, on repart
						train.start();
					}
				}
			}
		}
	}
}
