package algo.strategy;

import java.util.List;

import main.Variable;
import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Scenario 1, boucle de station
 * 
 * @throws BadCircuit
 * @throws ElementNotFoundException
 * @throws ID_Error
 */
public class Scenario1 extends ScenarioBase {

	public Scenario1(ICircuit circuit) {
		super(circuit);
	}

	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			// si c'est un capteur actif
			if (elt instanceof ICapteur && ((ICapteur) elt).getStatut()) {
				ICapteur capt = (ICapteur) elt;
				// on recupere la rame qui a declenche le capteur
				IRame train = (IRame) circuit.prec(capt);

				// on avance le train
				circuit.avancerTrain(train, capt);

				// on passe le feu au rouge
				capt.getFeu().setCouleur(FeuCouleur.red);

				// le prec au vert
				capt.feuPrec().get(0).setCouleur(FeuCouleur.green);

				// on stop le train
				train.stop(System.currentTimeMillis()
						+ rand.nextInt(Variable.ATT_MAX - Variable.ATT_MIN)
						+ Variable.ATT_MIN);

				capt.off();
			} else if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				// sinon si c'est un train en attente
				if (train.getEtat() == TrainAction.stop) {
					if (System.currentTimeMillis() >= train.getAttente()) {
						// si le tps d'attente est fini
						ICapteur capt = (ICapteur) circuit.suiv(train);
						if (capt.getFeu().getCouleur() == FeuCouleur.green) {
							// si le feu suivant est vert
							train.start();
						}
					}
				}
			}
		}
	}
}
