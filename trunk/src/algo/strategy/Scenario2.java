package algo.strategy;

import java.util.List;

import main.Variable;
import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Scenario 2, boucle troncon + station sans feu
 * 
 * @throws BadCircuit
 * @throws ElementNotFoundException
 * @throws ID_Error
 */
public class Scenario2 extends ScenarioBase {

	public Scenario2(ICircuit circuit) {
		super(circuit);
	}

	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			// si c'est un capteur actif
			if (elt instanceof ICapteur && ((ICapteur) elt).getStatut()) {
				ICapteur capt = (ICapteur) elt;

				// on recupere la rame qui a declenche le capteur
				IRame train = (IRame) circuit.prec(capt);

				// on avance le train
				circuit.avancerTrain(train, capt);

				// si on est en gare
				if (capt.estStation()) {
					// on stop le train
					train.stop(System.currentTimeMillis()
							+ rand.nextInt(Variable.ATT_MAX - Variable.ATT_MIN)
							+ Variable.ATT_MIN);
				} else {
					// on est sur un troncon
					// on passe le feu a rouge
					capt.getFeu().setCouleur(FeuCouleur.red);

					// le feu prec au vert
					capt.feuPrec().get(0).setCouleur(FeuCouleur.green);
				}

				// capteur traite -> off
				capt.off();

			} else if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				if (train.getEtat() == TrainAction.stop) {
					if ((train.getAttente() == 0)
							|| (System.currentTimeMillis() >= train
									.getAttente())) {
						// si le tps d'attente est null ou bien fini

						ICapteur capt = (ICapteur) circuit.prec(train);

						if (capt.estStation()
								&& capt.feuSuivant().get(0).getCouleur() == FeuCouleur.green
								|| !capt.estStation()) {
							// feu troncon + arret gare
							// ou arret fin troncon, prochain feu = vert
							// on repart
							train.start();
						}
					}
				}
			}
		}
	}

	@Override
	public void start() throws BadCircuit, ElementNotFoundException {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				if (((ICapteur) elt).estStation()
						&& circuit.prec(elt) instanceof ICapteur
						&& circuit.suiv(elt) instanceof ICapteur) {
					((ICapteur) circuit.prec(elt)).getFeu().setCouleur(
							FeuCouleur.green);
				}
			}
		}

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un train
			if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				ICapteur capt = (ICapteur) circuit.suiv(train);

				// si le feu suivant est vert
				if (capt.getFeu() != null) {
					if (capt.getFeu().getCouleur() == FeuCouleur.green) {
						train.start();
					}
				} else if (capt.feuSuivant().get(0).getCouleur() == FeuCouleur.green) {
					train.start();
				}
			}
		}
	}

}
