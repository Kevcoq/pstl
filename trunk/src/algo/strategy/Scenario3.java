package algo.strategy;

import java.util.List;

import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.enumeration.TrainDirection;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Scenario 3, aller-retour troncon
 * 
 * @throws BadCircuit
 * @throws ElementNotFoundException
 * @throws ID_Error
 */
public class Scenario3 extends ScenarioBase {
	private int nbTrain = 0;
	private TrainDirection tDir;

	public Scenario3(ICircuit circuit) {
		super(circuit);

		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			if (elt instanceof IRame) {
				tDir = ((IRame) elt).getDirection();
				nbTrain++;
			}
		}
		System.out.println("Nous partons dans le sens " + tDir + ".");
	}

	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error {
		// init
		List<IEltCircuit> eltCircuit = circuit.getList();
		int cptTrain = 0;

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			// si c'est un train
			if (elt instanceof IRame) {
				IRame train = (IRame) elt;

				// si il est a l'arret
				if (train.getEtat() == TrainAction.stop) {

					//
					// sens avant
					if (tDir == TrainDirection.forward) {
						ICapteur capt = (ICapteur) circuit.suiv(train);
						if (!capt.suiv().isEmpty()
								&& capt.feuSuivant().get(0).getCouleur() == FeuCouleur.green) {
							// si le feu suivant est vert
							train.start();
						} else
							cptTrain++;
					}

					//
					// sens inverse
					else {
						ICapteur capt = (ICapteur) circuit.prec(train);
						if (!capt.prec().isEmpty()
								&& capt.feuPrec().get(0).getCouleur() == FeuCouleur.green) {
							// si le feu suivant est vert
							train.start();
						} else
							cptTrain++;
					}
				}
			} else {

				// si c'est un capteur actif
				if (elt instanceof ICapteur && ((ICapteur) elt).getStatut()) {
					ICapteur capt = (ICapteur) elt;

					// on passe le feu au rouge
					capt.getFeu().setCouleur(FeuCouleur.red);

					//
					//
					// sens normal
					if (tDir == TrainDirection.forward) {
						// on recupere la rame qui a declenche le capteur
						IRame train = (IRame) circuit.prec(capt);

						if (!capt.suiv().isEmpty())
							// on avance le train
							circuit.avancerTrain(train, capt);

						// feu prec a vert
						capt.feuPrec().get(0).setCouleur(FeuCouleur.green);

						if (capt.suiv().isEmpty()
								|| capt.feuSuivant().get(0).getCouleur() == FeuCouleur.red) {
							// si le feu d'apres est rouge ou qu'on est au bout
							// de la ligne -> on stop le train
							train.stop();
						}
					}

					//
					//
					// sens inverse
					else {
						// on recupere la rame qui a declenche le capteur
						IRame train = (IRame) circuit.suiv(capt);

						if (!capt.prec().isEmpty())
							// on recule le train
							circuit.reculerTrain(train, capt);

						// feu suivant a vert
						capt.feuSuivant().get(0).setCouleur(FeuCouleur.green);

						if (capt.prec().isEmpty()
								|| capt.feuPrec().get(0).getCouleur() == FeuCouleur.red) {
							// si le feu d'avant est rouge ou qu'on est au bout
							// de la ligne -> on stop le train
							train.stop();
						}
					}
					capt.off();
				}
			}
		}

		//
		//
		//
		//
		if (cptTrain == nbTrain) {
			// changer de sens
			System.out.println("On change de sens");
			tDir = (tDir == TrainDirection.forward ? TrainDirection.backward
					: TrainDirection.forward);

			for (int i = 0; i < eltCircuit.size(); i++) {
				IEltCircuit elt = eltCircuit.get(i);

				if (elt instanceof IRame) {
					((IRame) elt).setDirection(tDir);
				}
			}
			start();
		}
	}

	@Override
	public void start() throws BadCircuit, ElementNotFoundException {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				if ((tDir == TrainDirection.forward ? circuit.suiv(elt)
						: circuit.prec(elt)) instanceof ICapteur) {
					((ICapteur) elt).getFeu().setCouleur(FeuCouleur.green);
				}
			}
		}

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un train
			if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				ICapteur capt = (ICapteur) (tDir == TrainDirection.forward ? circuit
						.suiv(train) : circuit.prec(train));

				if (capt.getFeu().getCouleur() == FeuCouleur.green) {
					// si le feu est vert
					train.start();
				}
			}
		}
	}
}
