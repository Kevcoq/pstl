package algo.strategy;

import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;

/**
 * Interface des scenarios
 * 
 * @author Kevcoq
 * 
 */
public interface IScenario {
	/**
	 * Algorithme de modification du circuit
	 * 
	 * @throws BadCircuit
	 *             exception circuit mal forme
	 * @throws ElementNotFoundException
	 * @throws ID_Error
	 */
	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error;

	/**
	 * lancement des trains
	 * 
	 * @throws BadCircuit
	 * @throws ElementNotFoundException
	 */
	void start() throws BadCircuit, ElementNotFoundException;
}
