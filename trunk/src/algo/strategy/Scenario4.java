package algo.strategy;

import java.util.List;

import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Scenario 4, ligne puis aiguillage avec boucle
 * 
 * @throws BadCircuit
 * @throws ElementNotFoundException
 * @throws ID_Error
 */
public class Scenario4 extends ScenarioBase {

	public Scenario4(ICircuit circuit) {
		super(circuit);
	}

	@Override
	public void execute() throws BadCircuit, ElementNotFoundException, ID_Error {
		List<IEltCircuit> eltCircuit = circuit.getList();
		ICapteur capteur_prec = null;

		// Pour chaque elt
		for (int i = 0; i < eltCircuit.size(); i++) {
			IEltCircuit elt = eltCircuit.get(i);

			// si c'est un capteur actif
			if (elt instanceof ICapteur) {
				ICapteur capt = (ICapteur) elt;
				capteur_prec = capt;
				if (((ICapteur) elt).getStatut()) {

					// on recupere la rame qui a declenche le capteur
					IRame train = null;
					for (ICapteur tmp : capt.prec())
						if (circuit.suiv(tmp) instanceof IRame) {
							train = (IRame) circuit.suiv(tmp);
							if (train.getEtat() == TrainAction.start)
								break;
							else
								train = null;
						}
					if (train == null)
						throw new BadCircuit("pas de train qui me precede\n");

					// le feu prec au vert
					ICapteur capt_prec = (ICapteur) circuit.prec(train);
					if (capt_prec.getFeu() != null)
						capt_prec.getFeu().setCouleur(FeuCouleur.green);

					// on avance le train et le stop
					circuit.ajouterApres(train, capt.getId());
					train.stop();

					if ((capt.feuSuivant()).get(0).getCouleur() == FeuCouleur.green) {
						// si le feu d'apres est vert, on continue
						train.start();
						capt.feuSuivant().get(0).setCouleur(FeuCouleur.red);
					}
					// capteur traite -> off
					capt.off();
				}
			} else if (elt instanceof IRame) {
				IRame train = (IRame) elt;

				// si c'est un train a l'arret
				if (train.getEtat() == TrainAction.stop) {
					ICapteur capt = capteur_prec.suiv().get(0);
					if (capt.getFeu().getCouleur() == FeuCouleur.green) {
						// si le feu suivant est vert, on repart
						capt.getFeu().setCouleur(FeuCouleur.red);
						train.start();
					}
				}
			}
		}
	}

	@Override
	public void start() throws BadCircuit, ElementNotFoundException {
		List<IEltCircuit> eltCircuit = circuit.getList();

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un capteur
			if (elt instanceof ICapteur) {
				if (circuit.suiv(elt) instanceof ICapteur) {
					((ICapteur) elt).getFeu().setCouleur(FeuCouleur.green);
				}
			}
		}

		// Pour chaque elt
		for (IEltCircuit elt : eltCircuit) {
			// si c'est un train
			if (elt instanceof IRame) {
				IRame train = (IRame) elt;
				ICapteur capt = (ICapteur) circuit.suiv(train);

				if (capt.getFeu().getCouleur() == FeuCouleur.green) {
					// si le feu suivant est vert
					train.start();
					capt.getFeu().setCouleur(FeuCouleur.red);
				}
			}
		}
	}
}
