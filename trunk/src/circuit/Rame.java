package circuit;

import circuit.enumeration.TrainAction;
import circuit.enumeration.TrainDirection;
import circuit.inter.IRame;

/**
 * Implem des rames
 * 
 * @author Kevcoq
 * 
 */
public class Rame extends EltCircuit implements IRame {
	// attribut
	private TrainAction roule;
	private TrainDirection dir;
	private long attente;

	// pour le set
	private boolean changement = false;

	/**
	 * Construit une rame
	 * 
	 * @param id
	 *            identifiant de l'element
	 * @param dir
	 *            direction de la rame
	 * @param roule
	 */
	protected Rame(String id, TrainDirection dir, TrainAction roule) {
		super(id);
		this.roule = roule;
		this.dir = dir;
	}

	/**
	 * Construit une rame avec la direction en avant
	 * 
	 * @param id
	 */
	protected Rame(String id) {
		this(id, TrainDirection.forward, TrainAction.stop);
	}

	@Override
	public void stop() {
		stop(0);
	}

	@Override
	public void stop(long attente) {
		roule = TrainAction.stop;
		this.attente = attente;
		changement = true;
	}

	@Override
	public void start() {
		roule = TrainAction.start;
		changement = true;
	}

	@Override
	public TrainAction getEtat() {
		return roule;
	}

	@Override
	public long getAttente() {
		return attente;
	}

	@Override
	public TrainDirection getDirection() {
		return dir;
	}

	@Override
	public void setDirection(TrainDirection dir) {
		this.dir = dir;
		changement = true;
	}

	public String toString() {
		return "Train\t\t-> " + super.toString() + "\t| roule : " + roule
				+ "\t| sens : " + dir + " | att : " + attente + "\n";
	}

	@Override
	public String getXML() {
		return "<train " + super.getXML() + " action=\"" + roule
				+ "\" direction=\"" + dir + "\"/>";
	}

	@Override
	public String toXML() {
		return getXML();
	}

	// pour le set
	@Override
	public boolean changement() {
		return changement;
	}

	@Override
	public void resetChangement() {
		changement = false;
	}

	@Override
	public IRame clone() {
		Rame copie;
		copie = (Rame) super.clone();
		return copie;
	}
}
