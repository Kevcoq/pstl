package circuit;

import circuit.inter.IEltCircuit;

/**
 * Implem abstraite d'un element du circuit
 * 
 * @author Kevcoq
 * 
 */
public abstract class EltCircuit implements IEltCircuit {
	// attribut
	private String id;

	/**
	 * Construit un element circuit
	 * 
	 * @param id2
	 */
	protected EltCircuit(String id2) {
		super();
		this.id = id2;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public String toString() {
		return "id : " + id;
	}

	@Override
	public String getXML() {
		return "id=\"" + id.substring(1) + "\"";
	}

	@Override
	public IEltCircuit clone() {
		try {
			EltCircuit copie;
			copie = (EltCircuit) super.clone();
			return copie;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
