package circuit;

import java.util.List;

import circuit.inter.ICapteur;

/**
 * Implem un capteur canton
 * 
 * @author Kevcoq
 * 
 */
public class CapteurCanton extends AbstractCapteur {

	/**
	 * Construit un capteur canton
	 * 
	 * @param id
	 *            son identifiant
	 * @param obj_prec
	 *            le capteur precedant
	 * @param obj_suiv
	 *            le capteur suivant
	 */
	protected CapteurCanton(String id, List<ICapteur> obj_prec,
			List<ICapteur> obj_suiv) {
		super(id, obj_prec, obj_suiv);
	}

	/**
	 * Construit un capteur canton sans liaison
	 * 
	 * @param id
	 *            son identifiant
	 */
	protected CapteurCanton(String id) {
		super(id);
	}

	@Override
	public boolean estStation() {
		return false;
	}
}
