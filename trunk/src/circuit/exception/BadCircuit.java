package circuit.exception;

/**
 * Exception : le circuit a une incoherence
 * 
 * @author kevcoq
 * 
 */
public class BadCircuit extends Exception {

	/**
	 * Construit l'exception BadCircuit
	 * 
	 * @param string
	 *            le message
	 */
	public BadCircuit(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
