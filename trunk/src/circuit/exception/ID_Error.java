package circuit.exception;

/**
 * Exception si l'id n'est pas correcte
 * 
 * @author kevcoq
 * 
 */
public class ID_Error extends Exception {

	/**
	 * Construit l'exception ID_Error
	 * 
	 * @param string
	 *            le message
	 */
	public ID_Error(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
