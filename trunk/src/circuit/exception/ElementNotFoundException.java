package circuit.exception;

/**
 * Exception lorsque l'element est introuvable
 * 
 * @author kevcoq
 * 
 */
public class ElementNotFoundException extends Exception {

	/**
	 * Construit l'exception EltNotFound
	 * 
	 * @param string
	 *            le message
	 */
	public ElementNotFoundException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}