package circuit;

import java.util.ArrayList;
import java.util.List;

import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.enumeration.TrainDirection;
import circuit.exception.BadCircuit;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.ILed;
import circuit.inter.IRame;

/**
 * Factory du circuit
 * 
 * @author Kevcoq
 * 
 */
public class FactoryCircuit {
	/**
	 * Cr�er un feu
	 * 
	 * @param id
	 *            l'identifiant du capteur
	 * @param circuit
	 *            le circuit
	 * @param color
	 *            la couleur du feu
	 * @return un feu
	 * @throws ID_Error
	 *             l'identifiant donne n'appartient pas au circuit
	 */
	public static ILed createFeu(String id, ICircuit circuit, String color)
			throws ID_Error {
		// Gestion de la couleur du feu
		FeuCouleur couleur;
		if (color.equals("green"))
			couleur = FeuCouleur.green;
		else
			couleur = FeuCouleur.red;

		// Creation de la led
		ILed feu = new Leds(id, couleur);

		// Recherche du capteur et ajout de la lampe
		IEltCircuit e;
		if ((e = circuit.recherche_id(id)) != null && e instanceof ICapteur) {
			((ICapteur) e).setFeu(feu);
		} else {
			throw new ID_Error("id : " + id
					+ "ne correspond pas a un capteur\n");
		}
		return feu;
	}

	/**
	 * Cr�er un capteur
	 * 
	 * @param id
	 *            son identifiant
	 * @param circuit
	 *            le circuit
	 * @param estStation
	 *            station ou canton
	 * @param in
	 *            l'identifiant des capteurs precedent
	 * @param out
	 *            l'identifiant des capteurs suivant
	 * @return un capteur
	 * @throws ID_Error
	 *             si l'identifiant du capteur precedant ou suivant n'existe pas
	 *             dans le circuit
	 * @throws BadCircuit
	 *             si l'identifiant du capteur precedant ou suivant est null
	 */
	public static ICapteur createCapteur(String id, ICircuit circuit,
			boolean estStation, List<String> in, List<String> out)
			throws ID_Error, BadCircuit {
		// on recherche le capteur precedant et suivant
		List<ICapteur> obj_prec = new ArrayList<ICapteur>(), obj_suiv = new ArrayList<ICapteur>();
		for (String tmp : in)
			obj_prec.add((ICapteur) circuit.recherche_id(tmp));
		for (String tmp : out)
			obj_suiv.add((ICapteur) circuit.recherche_id(tmp));

		// si l'un des deux est null, erreur
		if ((in.isEmpty() ? false : obj_prec.get(0) == null)
				|| (out.isEmpty() ? false : obj_suiv.get(0) == null)) {
			throw new BadCircuit("prec ou suiv inexistant\n");
		}

		// On regarde si le capteur n'est pas deja creer sous sa forme minimal
		IEltCircuit e;
		if ((e = circuit.recherche_id(id)) != null) {
			// si le capteur existe deja, on lui ajoute le precedant et le
			// suivant
			if (e instanceof ICapteur) {
				ICapteur c = (ICapteur) e;
				if (!obj_prec.isEmpty())
					for (ICapteur tmp : obj_prec) {
						c.setPrec(tmp);
					}
				if (!obj_suiv.isEmpty())
					for (ICapteur tmp : obj_suiv) {
						c.setSuiv(tmp);
					}
				return c;
			} else {
				// si ce n'est pas un capteur -> erreur
				throw new ID_Error("l'identifiant : '" + id
						+ "' ne correspond pas a un capteur.\n");
			}
		}
		// sinon on creer un nouveau capteur
		else if (estStation) {
			// si capteur station
			return new CapteurStation(id, obj_prec, obj_suiv);
		} else {
			// si capteur canton
			return new CapteurCanton(id, obj_prec, obj_suiv);
		}
	}

	/**
	 * Construit un capteur minimal
	 * 
	 * @param id
	 *            son identifiant
	 * @param circuit
	 *            le circuit
	 * @param estStation
	 *            station ou canton
	 * @return un capteur minimal
	 * @throws ID_Error
	 *             si l'identifiant donne existe mais ne correspond pas a un
	 *             capteur
	 */
	public static ICapteur createCapteur(String id, ICircuit circuit,
			boolean estStation) throws ID_Error {
		// on recherche l'id dans le circuit
		IEltCircuit e;
		if ((e = circuit.recherche_id(id)) != null) {
			if (e instanceof ICapteur) {
				// il existe et c'est un capteur, on le retourne
				return (ICapteur) e;
			} else {
				// si ce n'est pas un capteur -> erreur
				throw new ID_Error("l'identifiant : '" + id
						+ "' ne correspond pas a un capteur.\n");
			}
		}
		// nouveau capteur
		else if (estStation) {
			// si station
			return new CapteurStation(id);
		} else {
			// sinon canton
			return new CapteurCanton(id);
		}
	}

	/**
	 * Creer une rame
	 * 
	 * @param id
	 *            son identifiant
	 * @param circuit
	 *            le circuit
	 * @return une rame
	 * @throws ID_Error
	 *             si l'id donne correspond a qqchose qui n'est pas un train
	 */
	public static IRame createTrain(String id, ICircuit circuit)
			throws ID_Error {
		// on recherche si l'identifiant existe
		IEltCircuit e;
		if ((e = circuit.recherche_id(id)) != null)
			if (e instanceof IRame) {
				// si c'est une rame on la retourne
				return (IRame) e;
			} else {
				// si c'est autre chose -> erreur
				throw new ID_Error("train " + id + "\n");
			}
		// sinon nouvelle rame
		return new Rame(id);
	}

	/**
	 * Cr�er une rame
	 * 
	 * @param id
	 *            son identifiant
	 * @param circuit
	 *            le circuit
	 * @param dir_train
	 *            la direction du train
	 * @param roule
	 *            l'etat du train start ou stop
	 * @return une rame
	 * @throws ID_Error
	 *             si l'id donne correspond a qqchose qui n'est pas un train
	 */
	public static IRame createTrain(String id, ICircuit circuit,
			String dir_train, boolean roule) throws ID_Error {
		// on definit la direction
		TrainDirection dir;
		if (dir_train.equals("backward"))
			dir = TrainDirection.backward;
		else
			dir = TrainDirection.forward;

		// on recherche l'identifiant
		IEltCircuit e;
		if ((e = circuit.recherche_id(id)) != null)
			if (e instanceof IRame) {
				// si c'est une rame, on definit ces attributs et on la retourne
				((IRame) e).setDirection(dir);
				if (roule)
					((IRame) e).start();
				else
					((IRame) e).stop();
				return (IRame) e;
			} else {
				// si ce n'est pas un train -> erreur
				throw new ID_Error("train " + id + "\n");
			}
		// sinon on creer
		if (roule) {
			// un train qui roule
			return new Rame(id, dir, TrainAction.start);
		} else {
			// un train a l'arret
			return new Rame(id, dir, TrainAction.stop);
		}
	}

	/**
	 * Creer un circuit vide
	 * 
	 * @return un circuit tout neuf et tout vide
	 */
	public static ICircuit createCircuit() {
		return new Circuit();
	}
}
