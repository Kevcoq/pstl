package circuit;

import circuit.enumeration.FeuCouleur;
import circuit.inter.ILed;

/**
 * Implem d'un feu
 * 
 * @author Kevcoq
 * 
 */
public class Leds extends EltCircuit implements ILed {
	// attribut
	private FeuCouleur color;

	// pour le set
	private boolean changement;

	/**
	 * Construit une led
	 * 
	 * @param id_capt
	 *            identifiant du capteur
	 * @param color
	 *            couleur du feu
	 */
	protected Leds(String id_capt, FeuCouleur color) {
		super(id_capt);
		this.color = color;
	}

	@Override
	public FeuCouleur getCouleur() {
		return color;
	}

	@Override
	public void setCouleur(FeuCouleur color) {
		if (this.color != color) {
			this.color = color;
			changement = true;
		}
	}

	public String toString() {
		return "Leds etat : " + color;
	}

	@Override
	public String getXML() {
		return "<light " + super.getXML() + " color=\"" + color + "\"/>";
	}

	@Override
	public String toXML() {
		return getXML();
	}

	// pour le set
	@Override
	public boolean changement() {
		return changement;
	}

	@Override
	public void resetChangement() {
		changement = false;
	}

	@Override
	public ILed clone() {
		Leds copie;
		copie = (Leds) super.clone();
		return copie;
	}
}
