package circuit;

import java.util.ArrayList;
import java.util.List;

import circuit.exception.BadCircuit;
import circuit.inter.ICapteur;
import circuit.inter.ILed;

/**
 * Implem abstraite d'un capteur
 * 
 * @author Kevcoq
 * 
 */
public abstract class AbstractCapteur extends EltCircuit implements ICapteur {
	// attribut
	private boolean statut;
	private List<ICapteur> prec;
	private List<ICapteur> suiv;
	private ILed feu;

	/**
	 * Construit un capteur
	 * 
	 * @param id
	 *            son identifiant
	 * @param obj_prec
	 *            le capteur precedant
	 * @param obj_suiv
	 *            le capteur suivant
	 */
	public AbstractCapteur(String id, List<ICapteur> obj_prec,
			List<ICapteur> obj_suiv) {
		super(id);
		if (this.prec == null)
			this.prec = new ArrayList<ICapteur>();
		if (this.suiv == null)
			this.suiv = new ArrayList<ICapteur>();

		this.statut = false;

		if (!obj_prec.isEmpty()) {
			this.prec.addAll(obj_prec);
			for (ICapteur tmp : obj_prec)
				tmp.setSuiv(this);
		}
		if (!obj_suiv.isEmpty())
			this.suiv.addAll(obj_suiv);
	}

	/**
	 * Construit un capteur
	 * 
	 * @param id
	 *            son identifiant
	 */
	public AbstractCapteur(String id) {
		super(id);
		this.statut = false;
		prec = new ArrayList<ICapteur>();
		suiv = new ArrayList<ICapteur>();
	}

	@Override
	public boolean getStatut() {
		return statut;
	}

	@Override
	public void off() {
		statut = false;
	}

	@Override
	public void on() {
		statut = true;
	}

	@Override
	public List<ICapteur> prec() {
		return prec;
	}

	@Override
	public List<ICapteur> suiv() {
		return suiv;
	}

	@Override
	public ILed getFeu() {
		return feu;
	}

	@Override
	public void setFeu(ILed feu) {
		this.feu = feu;
	}

	@Override
	public List<ILed> feuSuivant() throws BadCircuit {
		List<ICapteur> current = suiv();
		List<ILed> feux = new ArrayList<ILed>();
		while (feux.isEmpty() && !current.isEmpty()) {
			List<ICapteur> next = new ArrayList<ICapteur>();
			for (ICapteur capt : current) {
				next.addAll(capt.suiv());
				if (capt.getFeu() != null) {
					feux.add(capt.getFeu());
				}
			}
			current = next;

		}
		if (feux.isEmpty())
			throw new BadCircuit("absence de feu suivant\n");
		else
			return feux;

	}

	@Override
	public List<ILed> feuPrec() throws BadCircuit {
		List<ICapteur> current = prec();
		List<ILed> feux = new ArrayList<ILed>();
		while (feux.isEmpty() && !current.isEmpty()) {
			List<ICapteur> prev = new ArrayList<ICapteur>();
			for (ICapteur capt : current) {
				prev.addAll(capt.prec());
				if (capt.getFeu() != null) {
					feux.add(capt.getFeu());
				}
			}
			current = prev;
		}
		if (feux.isEmpty())
			throw new BadCircuit("absence de feu prec\n");
		else
			return feux;
	}

	@Override
	public void setPrec(ICapteur capteur) {
		if (!prec.contains(capteur))
			prec.add(capteur);
	}

	@Override
	public void setSuiv(ICapteur capteur) {
		if (!suiv.contains(capteur))
			suiv.add(capteur);
	}

	public String toString() {
		String s = "";
		if (prec != null) {
			for (ICapteur tmp : prec)
				s += "| prec_id : " + tmp.getId() + "\t";
		}
		if (suiv != null) {
			for (ICapteur tmp : suiv)
				s += "| suiv_id : " + tmp.getId() + "\t";
		}
		if (feu != null) {
			s += "<=> " + feu;
		}
		if (this instanceof CapteurStation) {
			return "Capteur Station\t-> " + super.toString() + "\t| actif : "
					+ statut + "\t" + s + "\n";
		} else {
			return "Capteur Canton\t-> " + super.toString() + "\t| actif : "
					+ statut + "\t" + s + "\n";
		}
	}

	@Override
	public String getXML() {
		if (this instanceof CapteurStation) {
			return "<capteur " + super.getXML() + " type=\"station\"/>";
		} else {
			return "<capteur " + super.getXML() + " type=\"canton\"/>";
		}
	}

	@Override
	public String toXML() {
		String strFeu = "";
		if (feu != null) {
			strFeu = "feu=\"" + feu.getCouleur() + "\"";
		}
		if (this instanceof CapteurStation) {
			return "<capteur " + super.getXML() + " type=\"station\" etat=\""
					+ statut + "\" " + strFeu + " />";
		} else {
			return "<capteur " + super.getXML() + " type=\"canton\" etat=\""
					+ statut + "\" " + strFeu + " />";
		}
	}

	@Override
	public ICapteur clone() {
		AbstractCapteur copie;
		copie = (AbstractCapteur) super.clone();
		copie.feu = feu.clone();

		copie.prec = new ArrayList<ICapteur>();
		for (ICapteur tmp : prec)
			copie.prec.add(tmp.clone());
		copie.suiv = new ArrayList<ICapteur>();
		for (ICapteur tmp : suiv)
			copie.suiv.add(tmp.clone());

		return copie;
	}

}
