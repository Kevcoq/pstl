package circuit.enumeration;

/**
 * Enumeration des couleur d'un feu : [ green ; red ]
 * 
 * @author kevcoq
 * 
 */
public enum FeuCouleur {
	green, red
}