package circuit.enumeration;

/**
 * Enumeration des actions d'un train : [ start ; stop ]
 * 
 * @author kevcoq
 * 
 */
public enum TrainAction {
	start, stop;
}
