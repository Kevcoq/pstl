package circuit.enumeration;

/**
 * Enumeration des directions d'un train : [ avant ; arriere ]
 * 
 * @author kevcoq
 * 
 */
public enum TrainDirection {
	forward, backward;
}
