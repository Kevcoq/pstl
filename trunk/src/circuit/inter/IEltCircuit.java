package circuit.inter;

/**
 * Def un elt minimum du circuit
 * 
 * @author kevcoq
 * 
 */
public interface IEltCircuit extends Cloneable {
	/**
	 * Renvoie l'id
	 * 
	 * @return l'identifiant de l'elt
	 */
	public String getId();

	/**
	 * Renvoie le xml
	 * 
	 * @return le xml de l'elt
	 */
	public String getXML();

	/**
	 * renvoie un xml affichage
	 * 
	 * @return un xml affichage
	 */
	public String toXML();

	public IEltCircuit clone();
}
