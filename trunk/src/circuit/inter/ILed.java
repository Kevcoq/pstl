package circuit.inter;

import circuit.enumeration.FeuCouleur;

/**
 * Interface des leds
 * 
 * @author kevcoq
 * 
 */
public interface ILed extends IEltCircuit, IChangement, Cloneable {
	/**
	 * Obtient la couleur du feu
	 * 
	 * @return la couleur du feu
	 */
	public FeuCouleur getCouleur();

	/**
	 * assigne une nouvelle couleur au feu
	 * 
	 * @param color
	 *            la nouvelle couleur
	 */
	public void setCouleur(FeuCouleur color);

	public ILed clone();
}
