package circuit.inter;

public interface IChangement {
	/**
	 * vrai si il y a un changement
	 * 
	 * @return vrai si changement d'etat
	 */
	public boolean changement();

	/**
	 * une fois les changement envoye au serveur, redevient faux
	 */
	public void resetChangement();
}
