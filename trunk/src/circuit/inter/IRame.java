package circuit.inter;

import circuit.enumeration.TrainAction;
import circuit.enumeration.TrainDirection;

/**
 * interface d'un train
 * 
 * @author kevcoq
 * 
 */
public interface IRame extends IEltCircuit, IChangement, Cloneable {
	/**
	 * Stop le train
	 */
	public void stop();

	/**
	 * Stop le train avec un delai (arret en gare)
	 * 
	 * @param l
	 *            le delai
	 */
	public void stop(long l);

	/**
	 * demarre le train
	 */
	public void start();

	/**
	 * Donne l'action du train (start|stop)
	 * 
	 * @return start ou stop
	 */
	public TrainAction getEtat();

	/**
	 * Permet de savoir la date de redemarrage
	 * 
	 * @return la date de depart mini
	 */
	public long getAttente();

	/**
	 * le sens du train (forward|backward)
	 * 
	 * @return forward ou backward
	 */
	public TrainDirection getDirection();

	/**
	 * assigne une nouvelle direction
	 * 
	 * @param dir
	 *            la nouvelle direction
	 */
	void setDirection(TrainDirection dir);

	public IRame clone();
}
