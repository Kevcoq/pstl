package circuit.inter;

import java.util.List;

import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;

/**
 * interface du circuit
 * 
 * @author kevcoq
 * 
 */
public interface ICircuit extends Cloneable, IChangement {
	/**
	 * definit le scenario
	 * 
	 * @param id
	 *            identifiant du scenario
	 */
	public void setScenario(int id);

	/**
	 * ajoute un element au circuit
	 * 
	 * @param o
	 *            l'element
	 */
	public void ajouter(IEltCircuit o);

	/**
	 * ajoute un element apres un autre
	 * 
	 * @param elt
	 *            l'element
	 * @param id
	 *            l'element prec
	 * @throws ID_Error
	 */
	public void ajouterApres(IEltCircuit elt, String id) throws ID_Error;

	/**
	 * ajoute un element avant un autre
	 * 
	 * @param elt
	 *            l'element
	 * @param id
	 *            l'element suiv
	 * @throws ID_Error
	 */
	public void ajouterAvant(IEltCircuit elt, String id) throws ID_Error;

	/**
	 * ajoute un element a la table id<->elt
	 * 
	 * @param elt
	 *            l'element
	 */
	public void put(IEltCircuit elt);

	/**
	 * trouve l'elt d'identifiant id ou renvoie null
	 * 
	 * @param id
	 *            l'identifiant
	 * @return l'elt | null
	 */
	public IEltCircuit recherche_id(String id);

	/**
	 * retourne le scenario
	 * 
	 * @return le numero du scenario
	 */
	public int getScenario();

	/**
	 * rend l'element prec sur le circuit
	 * 
	 * @param elt
	 *            l'elt de base
	 * @return le composant qui precede elt
	 * @throws ElementNotFoundException
	 *             si l'element est introuvable
	 * @throws BadCircuit
	 *             exception si les elts se suivent mal
	 */
	public IEltCircuit prec(IEltCircuit elt) throws ElementNotFoundException,
			BadCircuit;

	/**
	 * rend l'element suivant sur le circuit
	 * 
	 * @param elt
	 *            l'elt de base
	 * @return le composant qui suit elt
	 * @throws ElementNotFoundException
	 *             si l'element est introuvable
	 * @throws BadCircuit
	 *             exception si les elts se suivent mal
	 */
	public IEltCircuit suiv(IEltCircuit elt) throws ElementNotFoundException,
			BadCircuit;

	/**
	 * avance le train, echange sa pos et celle du capteur
	 * 
	 * @param train
	 *            le train
	 * @param capt
	 *            le capteur qu'il declenche
	 * @throws BadCircuit
	 * @throws ID_Error
	 */
	public void avancerTrain(IRame train, ICapteur capt) throws BadCircuit,
			ID_Error;

	/**
	 * recule le train, echange sa pos et celle du capteur
	 * 
	 * @param train
	 *            le train
	 * @param capt
	 *            le capteur qu'il declenche
	 * @throws BadCircuit
	 * @throws ID_Error
	 */
	public void reculerTrain(IRame train, ICapteur capt) throws BadCircuit,
			ID_Error;

	/**
	 * donne la liste des elements du circuit
	 * 
	 * @return list
	 */
	public List<IEltCircuit> getList();

	/**
	 * Renvoie un xml circuit
	 * 
	 * @return un xml circuit
	 */
	public String toXML();

	/**
	 * permet de synchro les threads - attente
	 */
	public void attendre();

	/**
	 * permet de synchro les threads - reprise
	 */
	public void reprendre();

	public ICircuit clone();
}
