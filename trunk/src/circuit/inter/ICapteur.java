package circuit.inter;

import java.util.List;

import circuit.exception.BadCircuit;

/**
 * interface des capteurs
 * 
 * @author kevcoq
 * 
 */
public interface ICapteur extends IEltCircuit, Cloneable {
	/**
	 * actif or not
	 * 
	 * @return true si activé
	 */
	public boolean getStatut();

	/**
	 * eteint le captteur
	 */
	public void off();

	/**
	 * allume le capteur
	 */
	public void on();

	/**
	 * vrai si le capteur est un capteur de station, gare
	 * 
	 * @return true si capteurStation
	 */
	public boolean estStation();

	/**
	 * renvoie la liste des capteurs prec
	 * 
	 * @return la liste des capteurs prec
	 */
	public List<ICapteur> prec();

	/**
	 * renvoie la liste des capteurs suivant
	 * 
	 * @return la liste des capteurs suivant
	 */
	public List<ICapteur> suiv();

	/**
	 * assigne le capteur prec
	 * 
	 * @param capteur
	 *            le capteur prec
	 */
	public void setPrec(ICapteur capteur);

	/**
	 * assigne le capteur suiv
	 * 
	 * @param capteur
	 *            le capteur suivant
	 */
	public void setSuiv(ICapteur capteur);

	/**
	 * renvoie le feu associe au capteur
	 * 
	 * @return le feu ou null
	 */
	public ILed getFeu();

	/**
	 * assigne le feu au capteur
	 * 
	 * @param feu
	 *            la led
	 */
	void setFeu(ILed feu);

	/**
	 * donne les feux suivant
	 * 
	 * @return les feux suivant
	 * @throws BadCircuit
	 *             exception si pas de feu
	 */
	public List<ILed> feuSuivant() throws BadCircuit;

	/**
	 * donne les feux prec
	 * 
	 * @return les feux prec
	 * @throws BadCircuit
	 *             exception si pas de feu
	 */
	public List<ILed> feuPrec() throws BadCircuit;

	public ICapteur clone();
}