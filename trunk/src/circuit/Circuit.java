package circuit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import circuit.enumeration.TrainDirection;
import circuit.exception.BadCircuit;
import circuit.exception.ElementNotFoundException;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Implem d'un circuit
 * 
 * @author Kevcoq
 * 
 */
public class Circuit implements ICircuit {
	// attribut
	private List<IEltCircuit> list;
	private HashMap<String, IEltCircuit> map;
	private int scenario;

	/**
	 * Constructeur
	 */
	protected Circuit() {
		super();
		map = new HashMap<String, IEltCircuit>();
		list = new ArrayList<IEltCircuit>();
	}

	@Override
	public void ajouter(IEltCircuit o) {
		map.put(o.getId(), o);
		list.add(o);
	}

	@Override
	public void ajouterApres(IEltCircuit elt, String id) throws ID_Error {
		if (list.contains(elt)) {
			list.remove(elt);
		}
		if (!map.containsKey(id)) {
			throw new ID_Error("id inexistant\n");
		}
		map.put(elt.getId(), elt);
		list.add(list.indexOf(map.get(id)) + 1, elt);
	}

	@Override
	public void ajouterAvant(IEltCircuit elt, String id) throws ID_Error {
		if (list.contains(elt)) {
			list.remove(elt);
		}
		if (!map.containsKey(id)) {
			throw new ID_Error("id inexistant\n");
		}
		map.put(elt.getId(), elt);
		list.add(list.indexOf(map.get(id)), elt);
	}

	@Override
	public void put(IEltCircuit o) {
		map.put(o.getId(), o);
	}

	@Override
	public IEltCircuit recherche_id(String id) {
		return map.get(id);
	}

	@Override
	public int getScenario() {
		return scenario;
	}

	@Override
	public void setScenario(int id) {
		scenario = id;
	}

	@Override
	public List<IEltCircuit> getList() {
		return list;
	}

	@Override
	public void avancerTrain(IRame train, ICapteur capt) throws BadCircuit,
			ID_Error {
		if (train.getDirection() == TrainDirection.forward) {
			int pTrain = list.indexOf(train), pCapt = list.indexOf(capt);
			if (pTrain == pCapt - 1
					|| (pTrain == list.size() - 1 && pCapt == 0)) {
				ajouterApres(train, capt.getId());
			} else {
				throw new BadCircuit("le train ne precede pas le capteur\n");
			}
		} else {
			throw new BadCircuit("le train ne precede pas le capteur\n");
		}
	}

	@Override
	public void reculerTrain(IRame train, ICapteur capt) throws BadCircuit,
			ID_Error {
		if (train.getDirection() == TrainDirection.backward) {
			int pTrain = list.indexOf(train), pCapt = list.indexOf(capt);
			if (pTrain == pCapt + 1
					|| (pTrain == 0 && pCapt == list.size() - 1)) {
				ajouterAvant(train, capt.getId());
			} else {
				throw new BadCircuit(
						"le train ne precede pas le capteur (sens inverse)\n");
			}
		} else {
			throw new BadCircuit("le train ne precede pas le capteur\n");
		}
	}

	@Override
	public IEltCircuit prec(IEltCircuit elt) throws ElementNotFoundException,
			BadCircuit {
		int pos = list.indexOf(elt);
		if (pos == -1) {
			throw new ElementNotFoundException("Element " + elt.getId()
					+ " not found in list\n");
		}
		if (pos == 0) {
			return list.get(list.size() - 1);
		} else {
			return list.get(pos - 1);
		}
	}

	@Override
	public IEltCircuit suiv(IEltCircuit elt) throws ElementNotFoundException,
			BadCircuit {
		int pos = list.indexOf(elt);
		if (pos == -1) {
			throw new ElementNotFoundException("Element " + elt.getId()
					+ " not found in list\n");
		}
		if (pos < list.size() - 1) {
			return list.get(pos + 1);
		} else {
			return list.get(0);
		}
	}

	public String toString() {
		String s = "";
		for (IEltCircuit c : list) {
			s += "\t" + c.toString();
		}
		return "Circuit :\n\tScenario : " + scenario + "\n" + s;
	}

	@Override
	public String toXML() {
		String s = "";
		for (IEltCircuit c : list) {
			s += "\t" + c.toXML();
		}
		return "<circuit scenario=\"" + scenario + "\" ><topo>" + s
				+ "</topo></circuit>";
	}

	// gestion des threads
	@Override
	public synchronized void attendre() {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void reprendre() {
		notifyAll();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ICircuit clone() {
		try {
			Circuit copie = (Circuit) super.clone();
			copie.list = new ArrayList<IEltCircuit>();
			for (int i = 0; i < list.size(); i++) {
				copie.list.add(list.get(i));
			}

			copie.map = (HashMap<String, IEltCircuit>) map.clone();
			return copie;
		} catch (CloneNotSupportedException cnse) {
			cnse.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean changement() {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) instanceof ICapteur) {
				ICapteur capt = (ICapteur) list.get(i);
				if (capt.getFeu() != null) {
					if (capt.getFeu().changement()) {
						return true;
					}
				}
			} else if (list.get(i) instanceof IRame) {
				if (((IRame) list.get(i)).changement()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void resetChangement() {
	}
}
