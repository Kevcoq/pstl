package circuit;

import java.util.List;

import circuit.inter.ICapteur;

/**
 * Implem d'un capteur station
 * 
 * @author Kevcoq
 * 
 */
public class CapteurStation extends AbstractCapteur {

	/**
	 * Construit un capteur station
	 * 
	 * @param id
	 *            son identifiant
	 * @param obj_prec
	 *            le capteur precedant
	 * @param obj_suiv
	 *            le capteur suivant
	 */
	protected CapteurStation(String id, List<ICapteur> obj_prec,
			List<ICapteur> obj_suiv) {
		super(id, obj_prec, obj_suiv);
	}

	/**
	 * Construit un capteur station
	 * 
	 * @param id
	 *            son identifiant
	 */
	protected CapteurStation(String id) {
		super(id);
	}

	@Override
	public boolean estStation() {
		return true;
	}

}
