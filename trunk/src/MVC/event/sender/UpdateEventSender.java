package MVC.event.sender;

import MVC.event.listener.UpdateEventListener;

/**
 * Interface des envoies d'evenementss
 * 
 * @author Kevcoq
 * 
 */
public interface UpdateEventSender {
	/**
	 * ajoute un ecouteur
	 * 
	 * @param listener
	 *            l'objet qui nous ecoute
	 */
	public void add(UpdateEventListener listener);

	/**
	 * envoie un signaux de mise a jour
	 */
	public void update();
}
