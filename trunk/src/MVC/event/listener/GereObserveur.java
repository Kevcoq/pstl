package MVC.event.listener;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import MVC.view.observeur.Observeur;

/**
 * Gestion des observeurs
 * 
 * @author Kevcoq
 * 
 */
public class GereObserveur extends JPanel implements UpdateEventListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8234745661650482L;

	// une liste d'observeur
	private ArrayList<Observeur> list;

	/**
	 * Constructeur
	 */
	public GereObserveur() {
		super();
		this.list = new ArrayList<Observeur>();
	}

	@Override
	public void add(Observeur obj) {
		list.add(obj);
	}

	@Override
	public void paint(Graphics g) {
		for (Observeur o : list)
			if (o != null)
				o.print(g);
	}

	@Override
	public void manageUpdate() {
		paintImmediately(getBounds());
	}
}
