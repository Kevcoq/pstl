package MVC.event.listener;

import MVC.view.observeur.Observeur;

/**
 * Interface de reception d'evenements
 * 
 * @author Kevcoq
 * 
 */
public interface UpdateEventListener {
	/**
	 * Mise a jour sur reception du signal update
	 */
	public void manageUpdate();

	/**
	 * Ajoute un observeur
	 * 
	 * @param obj
	 */
	public void add(Observeur obj);
}
