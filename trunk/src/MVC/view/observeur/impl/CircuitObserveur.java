package MVC.view.observeur.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import MVC.view.observeur.Observeur;
import circuit.enumeration.FeuCouleur;
import circuit.enumeration.TrainAction;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IEltCircuit;
import circuit.inter.IRame;

/**
 * Implem de l'observation du circuit
 * 
 * @author Kevcoq
 * 
 */
public class CircuitObserveur implements Observeur {
	// la police d'ecriture
	private Font font = new Font(" TimesRoman ", Font.PLAIN, 30);

	// attribut
	private ICircuit circuit;
	private int w, h, rayon;
	private int nbCapt = 0;

	// gestion des couleurs des trains
	private Color[] colorTrain = { Color.magenta, Color.blue, Color.orange,
			Color.pink, Color.yellow };
	private Map<IEltCircuit, Color> map = new HashMap<IEltCircuit, Color>();

	/**
	 * Constructeur
	 * 
	 * @param circuit
	 *            le circuit
	 * @param width
	 *            la largeur du panel
	 * @param height
	 *            la hauteur du panel
	 */
	public CircuitObserveur(ICircuit circuit, int width, int height) {
		super();
		this.circuit = circuit;
		w = width;
		h = height;
		rayon = (h - 50) / 2;

		List<IEltCircuit> l = circuit.getList();
		for (int i = 0, j = 0; i < l.size(); i++) {
			if (l.get(i) instanceof ICapteur) {
				nbCapt++;
			} else {
				map.put(l.get(i), colorTrain[j % colorTrain.length]);
				j++;
			}
		}
	}

	@Override
	public void print(Graphics g) {
		// dessine un rectangle de fond en gris clair
		g.setColor(Color.lightGray);
		g.fillRect(0, 0, w, h);

		// dessine un cercle en noir
		g.setColor(Color.black);
		g.drawOval(w / 2 - rayon, h / 2 - rayon, rayon * 2, rayon * 2);
		g.drawOval(w / 2 - rayon, h / 2 - rayon, rayon * 2 + 1, rayon * 2 + 1);
		g.drawOval(w / 2 - rayon, h / 2 - rayon, rayon * 2 - 1, rayon * 2 - 1);

		// on dessine tout les elements du circuit
		List<IEltCircuit> l = circuit.getList();
		double nb_capteur_courant = 0;
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i) instanceof ICapteur) {
				// si c'est un capteur, on augmente le nb de capteur
				nb_capteur_courant++;

				// on calcule la position sur le cercle
				int x_tmp = xCircle(w / 2, nb_capteur_courant / nbCapt), y_tmp = yCircle(
						h / 2, nb_capteur_courant / nbCapt);

				// on definit la couleur
				ICapteur capt = (ICapteur) l.get(i);
				if (capt.getFeu() != null) {
					if (capt.getFeu().getCouleur() == FeuCouleur.green) {
						g.setColor(Color.green);
					} else {
						g.setColor(Color.red);
					}
				} else {
					g.setColor(Color.orange);
				}

				// si le capteur est active, la forme est pleine

				// si c'est une station un cercle
				if (capt.estStation()) {
					if (capt.getStatut()) {
						g.fillRoundRect(x_tmp - 12, y_tmp - 12, 25, 25, 20, 20);
					} else {
						g.drawRoundRect(x_tmp - 12, y_tmp - 12, 25, 25, 20, 20);
					}
				} else {
					// sinon un carre
					if (capt.getStatut()) {
						g.fillRect(x_tmp - 12, y_tmp - 12, 25, 25);
					} else {
						g.drawRect(x_tmp - 12, y_tmp - 12, 25, 25);
						g.drawRect(x_tmp - 11, y_tmp - 11, 25, 25);
					}
				}
				// on ecrit le nom du capteur
				ecrire(g, x_tmp, y_tmp, capt.getId());
			}
			// si c'est un train
			else if (l.get(i) instanceof IRame) {
				// on recupere sa couleur dans la map < train ; couleur >
				IRame train = (IRame) l.get(i);
				g.setColor(map.get(train));

				// les coordonnees sur le cercle
				int x_tmp = xCircle(w / 2, (nb_capteur_courant + 0.5) / nbCapt), y_tmp = yCircle(
						h / 2, (nb_capteur_courant + 0.5) / nbCapt);

				// on dessine une croix sur le cercle
				for (int n = -2; n < 3; n++) {
					g.drawLine(x_tmp - 10 + n, y_tmp - 10, x_tmp + 10 + n,
							y_tmp + 10);
					g.drawLine(x_tmp + 10 + n, y_tmp - 10, x_tmp - 10 + n,
							y_tmp + 10);
				}
				// on ecrit son identifiant
				if (train.getEtat() == TrainAction.start) {
					ecrire(g, x_tmp, y_tmp, train.getId().toUpperCase());
				} else {
					ecrire(g, x_tmp, y_tmp, train.getId());
				}
			}
		}

	}

	/**
	 * Ecrit sur g au coordonnées (x;y), la chaine str
	 * 
	 * @param g
	 *            la zone de dessin
	 * @param x
	 *            l'abscisse
	 * @param y
	 *            l'ordonnee
	 * @param str
	 *            la chaine de caractere
	 */
	private void ecrire(Graphics g, int x, int y, String str) {
		g.setFont(font);
		g.setColor(Color.white);
		g.drawString(str, x, y);
	}

	/**
	 * Calcule l'abscisse
	 * 
	 * @param centreX
	 *            l'abscisse du centre du cercle
	 * @param fraction
	 *            le nombre de part du cercle a delimite
	 * @return l'abscisse
	 */
	private int xCircle(int centreX, double fraction) {
		return (int) (Math.cos(fraction * 2 * Math.PI) * rayon + centreX);
	}

	/**
	 * Calcule l'ordonnee
	 * 
	 * @param centreY
	 *            l'ordonnee du centre du cercle
	 * @param fraction
	 *            le nombre de part du cercle a delimite
	 * @return l'ordonnee
	 */
	private int yCircle(int centreY, double fraction) {
		return (int) (Math.sin(fraction * 2 * Math.PI) * rayon + centreY);
	}
}
