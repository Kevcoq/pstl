package MVC.view.observeur;

import java.awt.Graphics;

/**
 * Interface des observeurs
 * 
 * @author Kevcoq
 * 
 */
public interface Observeur {
	/**
	 * Dessine sur le graphique g
	 * 
	 * @param g
	 *            la zone de dessin
	 */
	public void print(Graphics g);
}
