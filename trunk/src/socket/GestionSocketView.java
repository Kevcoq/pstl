package socket;

import interfaceGraphique.Fenetre;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import socket.echange.LectureThread;
import socket.echange.Write;
import xml.PCF;
import xml.enumeration.PcfType;
import MVC.event.listener.GereObserveur;
import MVC.event.listener.UpdateEventListener;
import MVC.event.sender.UpdateEventSender;
import MVC.view.observeur.impl.CircuitObserveur;

/**
 * Gestion de la socket avec affichage en plus
 * 
 * @author Kevcoq
 * 
 */
public class GestionSocketView extends GestionSocket implements
		UpdateEventSender, ISocket {
	// liste des ecouteurs
	private ArrayList<UpdateEventListener> uEL = new ArrayList<UpdateEventListener>();

	/**
	 * Constructeur privee
	 * 
	 * @param circuit
	 *            le circuit
	 * @throws FileNotFoundException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private GestionSocketView() throws FileNotFoundException,
			ParserConfigurationException, SAXException {
		super();
	}

	/**
	 * initialisation
	 * 
	 * @param circuit
	 *            le circuit
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws FileNotFoundException
	 */
	public static void initGestionSocket() throws ParserConfigurationException,
			SAXException, FileNotFoundException {
		singleton = new GestionSocketView();
	}

	@Override
	public void add(UpdateEventListener listener) {
		uEL.add(listener);
	}

	@Override
	public void start() throws InitException, IOException {
		System.out
				.println("*ding dong* D�part imminent, pour sortir du train en cours de route, appuyez sur enter.");

		lecture = new LectureThread(circuit, in, log, enregistrementCircuit);
		lecture.start();

		// hello
		Write.writePCF(GestionSocket.getReqId(), PcfType.request,
				PCF.getHelloXML("kevin"));
		circuit.attendre();

		System.out.println("*siflet* Le train quitte la gare.");

		// creation de la fenetre et des observeurs
		Fenetre fen = new Fenetre();
		GereObserveur obs = new GereObserveur();
		obs.add(new CircuitObserveur(circuit, fen.getWidth(), fen.getHeight()));
		fen.addJP(obs);

		// ajout des observeurs a la sockets
		add(obs);

		// ajout d'un thread d'update
		Thread t = new Thread() {
			public void run() {
				while (true) {
					update();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();

		try {
			lecture.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		out.close();
		in.close();
	}

	@Override
	public void update() {
		for (UpdateEventListener o : uEL) {
			o.manageUpdate();
		}
	}

}
