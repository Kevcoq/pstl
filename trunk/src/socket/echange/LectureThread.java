package socket.echange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import algo.Analyse;
import circuit.inter.ICircuit;

/**
 * Thread de lecture du xml recu
 * 
 * @author Kevcoq
 * 
 */
public class LectureThread extends Thread {
	// circuit
	private ICircuit circuit;
	private List<String> rep;

	// FLUX
	private BufferedReader in;
	private PrintWriter log;
	private PrintWriter enregistrementCircuit;

	// gestion de la boucle
	private boolean run = true;

	/**
	 * Constructeur
	 * 
	 * @param circuit
	 *            le circuit
	 * @param in
	 *            le flux d'entree
	 * @param log
	 *            ecriture des logs
	 * @param enregistrementCircuit
	 *            ecriture du circuit
	 */
	public LectureThread(ICircuit circuit, BufferedReader in, PrintWriter log,
			PrintWriter enregistrementCircuit) {
		super();
		// try {
		this.circuit = circuit;

		this.in = in;
		this.log = log;

		// init log_circuit
		this.enregistrementCircuit = enregistrementCircuit;
		this.enregistrementCircuit.print("<affichage>");
		this.enregistrementCircuit.flush();
	}

	public void run() {
		try {
			rep = new ArrayList<String>();
			Analyse analyse = new Analyse(circuit, this);
			analyse.start();
			while (run) {
				rep.add(in.readLine());

				// ecriture de la chaine dans les logs
				log.println("<!-- Lecture : -->\n" + rep.get(rep.size() - 1));
				log.flush();

				circuit = analyse.getCircuitClone();

				// on ecrit l'etat du circuit dans les log_circuits
				enregistrementCircuit.print(circuit.toXML());
				enregistrementCircuit.flush();
			}
		} catch (IOException e) {
			if (run) {
				e.printStackTrace();
			} else {
				// si le thread est interrompu avec run a faux, ce n'est pas une
				// exception
				// on fini l'ecriture du fichier log_circuit
				enregistrementCircuit.print("</affichage>");
				enregistrementCircuit.flush();
			}
		}
	}

	/**
	 * Permet de mettre le boolean de controle de la boucle a faux
	 */
	public void quit() {
		run = false;
	}

	/**
	 * renvoie la 1er reponse en attente
	 * 
	 * @return la 1er reponse en attente
	 */
	public String getRep() {
		if (rep.size() > 0) {
			return rep.remove(0);
		} else {
			return null;
		}
	}
}
