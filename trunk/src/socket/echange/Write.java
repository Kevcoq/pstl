package socket.echange;

import java.io.PrintWriter;

import socket.InitException;
import xml.PCF;
import xml.enumeration.PcfType;

/**
 * Permet d'envoyer des donnees a la socket
 * 
 * @author Kevcoq
 * 
 */
public class Write {
	// singleton
	private static Write instance;

	// flux de sortie
	private PrintWriter out;

	// log
	private PrintWriter log;

	/**
	 * Constructeur privee
	 * 
	 * @param out
	 *            le flux de sortie
	 * @param log
	 *            le flux des logs
	 */
	private Write(PrintWriter out, PrintWriter log) {
		super();
		this.out = out;
		this.log = log;
	}

	/**
	 * Initialise l'instance
	 * 
	 * @param out
	 *            le flux de sortie
	 * @param log
	 *            le flux des logs xml
	 * @throws InitException
	 *             si l'instance est deja initialise
	 */
	public static void init(PrintWriter out, PrintWriter log)
			throws InitException {
		if (instance == null) {
			instance = new Write(out, log);
		} else {
			throw new InitException("write deja init\n");
		}
	}

	/**
	 * Envoie une trame xml PCF
	 * 
	 * @param reqid
	 *            l'id du message
	 * @param type
	 *            le type du message [ request ; answer ; advise ]
	 * @param txt
	 *            le texte du message
	 * @throws InitException
	 *             exception si l'instance n'est pas initialise
	 */
	public static void writePCF(String reqid, PcfType type, String txt)
			throws InitException {
		if (instance == null) {
			throw new InitException("write non init\n");
		} else {
			instance.privateWrite(reqid, type, txt);
		}
	}

	/**
	 * Envoie une trame xml PCF
	 * 
	 * @param reqid
	 *            l'id du message
	 * @param type
	 *            le type du message [ request ; answer ; advise ]
	 * @param txt
	 *            le texte du message
	 */
	private void privateWrite(String reqid, PcfType type, String txt) {
		String s = PCF.getPCF(reqid, type, txt);
		out.println(s);
		out.flush();

		log.println("<!-- Ecriture :-->\n" + s);
		log.flush();
	}
}
