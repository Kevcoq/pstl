package socket;

/**
 * Exception singleton non initialise
 * 
 * @author Kevcoq
 * 
 */
public class InitException extends Exception {

	/**
	 * exception non init sockettools
	 * 
	 * @param string
	 *            le msg
	 */
	public InitException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
