package socket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import main.Variable;

import org.xml.sax.SAXException;

import socket.echange.LectureThread;
import socket.echange.Write;
import xml.PCF;
import xml.enumeration.PcfType;
import circuit.FactoryCircuit;
import circuit.inter.ICircuit;

/**
 * La socket avec les fctions adequate
 * 
 * @author Kevcoq
 * 
 */
public class GestionSocket implements ISocket {
	// singleton
	protected static ISocket singleton = null;

	// id unique
	private static int reqid = 0;

	// flux in | out
	private Socket socket = null;
	protected PrintWriter out = null;
	protected BufferedReader in = null;

	// lecture in
	protected LectureThread lecture;

	// circuit
	protected ICircuit circuit;

	// Log
	protected PrintWriter log;
	protected PrintWriter enregistrementCircuit;

	/**
	 * Constructeur
	 * 
	 * @param circuit
	 *            le circuit
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws FileNotFoundException
	 */
	protected GestionSocket() throws ParserConfigurationException,
			SAXException, FileNotFoundException {
		super();
		this.circuit = FactoryCircuit.createCircuit();
		this.log = new PrintWriter(new File(Variable.var().log));
		this.enregistrementCircuit = new PrintWriter(new File(
				Variable.var().enregistrementCircuit));
	}

	/**
	 * initialise gestionSocket
	 * 
	 * @param circuit
	 *            le circuit
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws FileNotFoundException
	 */
	public static void initGestionSocket() throws ParserConfigurationException,
			SAXException, FileNotFoundException {
		singleton = new GestionSocket();
	}

	/**
	 * donne le singleton
	 * 
	 * @return le singleton
	 * @throws InitException
	 */
	public static ISocket get() throws InitException {
		if (singleton == null) {
			throw new InitException("singleton non init\n");
		}
		return singleton;
	}

	/**
	 * calcule un id non pris
	 * 
	 * @return un id unique
	 */
	public static String getReqId() {
		return "" + reqid++;
	}

	@Override
	public void connexion() throws UnknownHostException, IOException,
			InitException {
		System.out.println("Demande de connexion");
		socket = new Socket(Variable.var()._host, Variable.var()._port);
		System.out.println("Connexion etablie.");

		out = new PrintWriter(socket.getOutputStream());
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		Write.init(out, log);
	}

	@Override
	public void start() throws InitException, IOException {
		System.out
				.println("*ding dong* D�part imminent, pour sortir du train en cours de route, appuyez sur enter.");

		lecture = new LectureThread(circuit, in, log, enregistrementCircuit);
		lecture.start();

		// hello
		Write.writePCF(GestionSocket.getReqId(), PcfType.request,
				PCF.getHelloXML("kevin"));
		circuit.attendre();

		System.out.println("*siflet* Le train quitte la gare.");

		Scanner scan = new Scanner(System.in);
		// while (!scan.nextLine().equals("quit")) {
		// }
		scan.nextLine();
		scan.close();
		close();

		try {
			lecture.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		out.close();
		in.close();
	}

	@Override
	public void close() throws IOException, InitException {
		// fermeture
		lecture.quit();
		System.out.println("Fermeture connexion.");
		Write.writePCF(getReqId(), PcfType.request, PCF.getByeXML());
		socket.close();
	}

	public LectureThread getLecture() {
		return lecture;
	}
}
