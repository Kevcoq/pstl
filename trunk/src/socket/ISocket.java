package socket;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Interface des gestionnaires de la socket
 * 
 * @author Kevcoq
 * 
 */
public interface ISocket {

	/**
	 * Fermeture de la connexion
	 * 
	 * @throws IOException
	 *             exception
	 * @throws InitException
	 */
	public void close() throws IOException, InitException;

	/**
	 * Connexion a la socket
	 * 
	 * @throws UnknownHostException
	 *             exception
	 * @throws IOException
	 *             exception
	 * @throws InitException
	 */
	public void connexion() throws UnknownHostException, IOException,
			InitException;

	/**
	 * Initialise et lance la simulation
	 * 
	 * @throws IOException
	 * @throws InitException
	 */
	public void start() throws InitException, IOException;

}
