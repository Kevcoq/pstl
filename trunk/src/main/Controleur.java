package main;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import socket.GestionSocket;
import socket.ISocket;
import socket.InitException;

/**
 * Le controleur, classe principale
 * 
 * @author kevcoq
 * 
 */
public class Controleur {

	/**
	 * La methode principales
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// on regarde les variables
			Variable.swap(args);

			// socket
			GestionSocket.initGestionSocket();
			ISocket socket = GestionSocket.get();
			socket.connexion();

			// go
			socket.start();

			// exception
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InitException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
}
