package main;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import socket.GestionSocketView;
import socket.InitException;

/**
 * Le controleur avec interface graphique, classe principale
 * 
 * @author kevcoq
 * 
 */
public class ControleurIHM {

	public static void main(String[] args) {
		try {
			// on regarde les variables
			Variable.swap(args);

			// Creation de la socket
			GestionSocketView.initGestionSocket();
			GestionSocketView socket = (GestionSocketView) GestionSocketView
					.get();
			socket.connexion();

			// go
			socket.start();

			// exception
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InitException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
