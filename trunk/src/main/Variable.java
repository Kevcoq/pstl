package main;

import socket.InitException;

/**
 * Contient les variables globales
 * 
 * @author Kevcoq
 * 
 */
public class Variable {
	// singleton
	private static Variable instance;

	// les variables
	public String log = "log/log.xml";
	public String enregistrementCircuit = "log/circuit.xml";
	// 55556 (Scenario 2) [OK]
	// 55557 (Scenario 1) [OK]
	// 55558 (Scenario 4) [OK]
	public int _port = 55558;
	// public String _host = "grimau.dyndns-free.com"; // ancienne adresse
	public String _host = "www.grimau.dynamic-dns.net";

	// tps d'attente
	public static int ATT_MAX = 5;
	public static int ATT_MIN = 2;

	/**
	 * permet de changer les variables en fonction des arguments donne au
	 * programme
	 * 
	 * @param args
	 *            la liste des parametres
	 * @throws InitException
	 */
	public static void swap(String[] args) throws InitException {
		// TODO
		if (instance != null) {
			throw new InitException(
					"Variable init avant l'ajustement des variables\n");
		}

		instance = new Variable();

		for (int i = 0; i < args.length; i = i + 2) {
			if (args[i].equals("-port")) {
				instance._port = Integer.parseInt(args[i + 1]);
			} else if (args[i].equals("-host")) {
				instance._host = args[i + 1];
			} else if (args[i].equals("-log")) {
				instance.log = args[i + 1];
			} else if (args[i].equals("-circuit")) {
				instance.enregistrementCircuit = args[i + 1];
			}
		}
	}

	/**
	 * Constructeur privee
	 */
	private Variable() {
		super();
	}

	/**
	 * Rend l'instance variable
	 * 
	 * @return
	 */
	public static Variable var() {
		if (instance == null) {
			instance = new Variable();
		}
		return instance;
	}
}
