package main.old_HS;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import xml.CircuitToXML;
import xml.IXML;
import xml.PCF;
import xml.Parseur;
import xml.enumeration.PcfType;
import circuit.FactoryCircuit;
import circuit.exception.BadCircuit;
import circuit.inter.ICircuit;

/**
 * OLD - Test le parseur sur des messages bien precis, comme un capteur ou un
 * feu
 * 
 * @author Kevcoq
 * 
 */
public class TestParseur {

	/**
	 * @param args
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 */
	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException {
		try {
			ICircuit circuit = FactoryCircuit.createCircuit();

			// Sax
			SAXParserFactory fabrique = SAXParserFactory.newInstance();
			SAXParser parseur = fabrique.newSAXParser();

			File scenario = new File("xml/e_scenario.xml");
			File topo = new File("xml/e_topo.xml");
			File train = new File("xml/e_train.xml");
			File feu = new File("xml/e_feu.xml");
			File up = new File("xml/e_up.xml");
			File set = new File("xml/e_set.xml");

			DefaultHandler gestionnaire = new Parseur(circuit);
			// scenario 1
			parseur.parse(scenario, gestionnaire);
			// c1 -> c2 -> s1 -> c3
			parseur.parse(topo, gestionnaire);
			// c3 -> t1 -> c1 && c2 -> t2 -> s1
			parseur.parse(train, gestionnaire);
			// l1(G) && l2(R) && l3(R)
			parseur.parse(feu, gestionnaire);
			// up => c1 && s1
			parseur.parse(up, gestionnaire);
			// roule t2 && l2(G)
			parseur.parse(set, gestionnaire);

			IXML xml = new CircuitToXML(circuit);

			// Affichage circuit
			System.out
					.println("================= affichage du circuit =============\n"
							+ circuit);

			// Affichage et sauvegarde xml_pcf
			String text_pcf = PCF.getPCF("pcf1", PcfType.request,
					xml.getScenarioXML() + xml.getTopoXML() + xml.getInitXML()
							+ xml.getLightsXML());
			File sv_pcf = new File("xml/sv_pcf.xml");
			PrintWriter pw_pcf = new PrintWriter(sv_pcf);
			pw_pcf.write(text_pcf);
			pw_pcf.flush();
			pw_pcf.close();
			System.out
					.println("=================     xml     ================\n"
							+ text_pcf);

		} catch (BadCircuit e) {
			e.printStackTrace();
		}
	}

}
