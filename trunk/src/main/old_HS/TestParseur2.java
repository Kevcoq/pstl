package main.old_HS;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import xml.CircuitToXML;
import xml.IXML;
import xml.PCF;
import xml.Parseur;
import xml.enumeration.PcfType;
import circuit.FactoryCircuit;
import circuit.exception.BadCircuit;
import circuit.inter.ICircuit;

/**
 * OLD - Test du parseur sur d'autre critere
 * 
 * @author Kevcoq
 * 
 */
public class TestParseur2 {

	/**
	 * @param args
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 */
	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException {
		try {
			ICircuit circuit = FactoryCircuit.createCircuit();

			// Sax
			SAXParserFactory fabrique = SAXParserFactory.newInstance();
			SAXParser parseur = fabrique.newSAXParser();

			File pcf = new File("xml/sv_pcf.xml");

			DefaultHandler gestionnaire = new Parseur(circuit);
			parseur.parse(pcf, gestionnaire);

			IXML xml = new CircuitToXML(circuit);

			// Affichage circuit
			System.out
					.println("================= affichage du circuit =============\n"
							+ circuit);

			// Affichage et sauvegarde xml_pcf
			String text_pcf = PCF.getPCF("pcf1", PcfType.request,
					xml.getScenarioXML() + xml.getTopoXML() + xml.getInitXML()
							+ xml.getLightsXML());
			File sv_pcf = new File("xml/sv_pcf.xml");
			PrintWriter pw_pcf = new PrintWriter(sv_pcf);
			pw_pcf.write(text_pcf);
			pw_pcf.flush();
			pw_pcf.close();
			System.out
					.println("=================     xml     ================\n"
							+ text_pcf);

		} catch (BadCircuit e) {
			e.printStackTrace();
		}
	}

}
