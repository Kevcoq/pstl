package main.old_HS;

import interfaceGraphique.Fenetre;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import xml.Parseur;
import MVC.event.listener.GereObserveur;
import MVC.view.observeur.impl.CircuitObserveur;
import circuit.FactoryCircuit;
import circuit.enumeration.FeuCouleur;
import circuit.exception.BadCircuit;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IRame;

/**
 * OLD - Test interface graphique avec quelque mouvement predefini
 * 
 * @author Kevcoq
 * 
 */
public class TestIHM {

	/**
	 * @param args
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws BadCircuit
	 * @throws ID_Error
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException, BadCircuit, ID_Error,
			InterruptedException {
		ICircuit circuit = FactoryCircuit.createCircuit();

		// Sax
		SAXParserFactory fabrique = SAXParserFactory.newInstance();
		SAXParser parseur = fabrique.newSAXParser();

		File pcf = new File("xml/e_circuit.xml");

		DefaultHandler gestionnaire = new Parseur(circuit);
		parseur.parse(pcf, gestionnaire);

		Thread.sleep(100);
		Fenetre fen = new Fenetre();

		GereObserveur obs = new GereObserveur();
		obs.add(new CircuitObserveur(circuit, fen.getWidth(), fen.getHeight()));
		fen.addJP(obs);

		obs.manageUpdate();
		// aff classique
		Thread.sleep(1000);

		// green 1
		System.out.println("green 1");
		((ICapteur) circuit.recherche_id("c1")).getFeu().setCouleur(
				FeuCouleur.green);
		obs.manageUpdate();
		Thread.sleep(1000);

		// start t2
		System.out.println("start t2");
		((IRame) circuit.recherche_id("t2")).start();
		obs.manageUpdate();
		Thread.sleep(1000);

		// up 3
		System.out.println("up c1");
		((ICapteur) circuit.recherche_id("c1")).on();
		obs.manageUpdate();
		Thread.sleep(1000);

		// red 1
		System.out.println("red 1");
		((ICapteur) circuit.recherche_id("c1")).getFeu().setCouleur(
				FeuCouleur.red);
		obs.manageUpdate();
		Thread.sleep(1000);

		// avance t2
		System.out.println("avance t2");
		circuit.avancerTrain((IRame) circuit.recherche_id("t2"),
				(ICapteur) circuit.recherche_id("c3"));
		obs.manageUpdate();
		Thread.sleep(1000);

		// green 3
		System.out.println("green 3");
		((ICapteur) circuit.recherche_id("c3")).getFeu().setCouleur(
				FeuCouleur.green);
		obs.manageUpdate();
		Thread.sleep(1000);

	}
}
