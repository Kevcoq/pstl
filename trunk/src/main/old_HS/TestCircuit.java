package main.old_HS;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import xml.Parseur;
import circuit.FactoryCircuit;
import circuit.exception.BadCircuit;
import circuit.exception.ID_Error;
import circuit.inter.ICapteur;
import circuit.inter.ICircuit;
import circuit.inter.IRame;

/**
 * OLD - test parsage du circuit
 * 
 * @author Kevcoq
 * 
 */
public class TestCircuit {

	/**
	 * parse un circuit statique en phase de test
	 * 
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BadCircuit
	 * @throws ID_Error
	 */
	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException, BadCircuit, ID_Error {
		ICircuit circuit = FactoryCircuit.createCircuit();

		// Sax
		SAXParserFactory fabrique = SAXParserFactory.newInstance();
		SAXParser parseur = fabrique.newSAXParser();

		File pcf = new File("xml/e_circuit.xml");

		DefaultHandler gestionnaire = new Parseur(circuit);
		parseur.parse(pcf, gestionnaire);

		// Affichage circuit
		System.out
				.println("================= affichage du circuit =============\n"
						+ circuit);

		circuit.avancerTrain((IRame) circuit.recherche_id("t2"),
				(ICapteur) circuit.recherche_id("c3"));
		// Affichage circuit
		System.out
				.println("================= affichage du circuit =============\n"
						+ circuit);

	}

}
