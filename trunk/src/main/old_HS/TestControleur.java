package main.old_HS;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import xml.PCF;
import xml.Parseur;
import xml.enumeration.PcfType;
import circuit.FactoryCircuit;
import circuit.inter.ICircuit;

/**
 * OLD - Test connexion, parsage du circuit
 * 
 * La structure par la suite a change, avec une decoupe plus approfondi
 * 
 * @author kevcoq
 * 
 */
public class TestControleur {
	// attribut
	public static Socket socket = null;
	private static PrintWriter out = null;
	private static BufferedReader in = null;
	public static Thread lecture;
	private static int reqid = 0;

	public static void main(String[] args) throws SAXException,
			ParserConfigurationException {

		try {
			connexion();

			// Sax
			SAXParserFactory fabrique = SAXParserFactory.newInstance();
			SAXParser parseur = fabrique.newSAXParser();

			// circuit
			ICircuit circuit = FactoryCircuit.createCircuit();
			DefaultHandler gestionnaire = new Parseur(circuit);

			init(gestionnaire, parseur);

			// Affichage circuit
			System.out
					.println("================= affichage du circuit =============\n"
							+ circuit);

			close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * calcule un id non pris
	 * 
	 * @return un id unique
	 */
	private static String getReqId() {
		return "" + reqid++;
	}

	/**
	 * Connexion a la socket
	 * 
	 * @param in
	 *            flux d'entree
	 * @param out
	 *            flux de sortie
	 * @throws UnknownHostException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	private static void connexion() throws UnknownHostException, IOException {
		System.out.println("Demande de connexion");
		socket = new Socket("grimau.dyndns-free.com", 55557);
		System.out.println("Connexion etablie.");

		out = new PrintWriter(socket.getOutputStream());
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	/**
	 * Initialisation
	 * 
	 * @param in
	 *            flux d'entree
	 * @param out
	 *            flux de sortie
	 * @param gestionnaire
	 *            SAX
	 * @param parseur
	 *            handler
	 * @throws IOException
	 *             exception
	 * @throws SAXException
	 *             exception
	 */
	private static void init(DefaultHandler gestionnaire, SAXParser parseur)
			throws IOException, SAXException {
		// hello
		String hello = PCF.getPCF(getReqId(), PcfType.request,
				PCF.getHelloXML("kevin"));
		out.println(hello);
		out.flush();

		// parse rep
		String rep = PCF.getDTD(in.readLine());
		File sv_pcf = new File("trunk/xml/sv_pcf.xml");
		PrintWriter pw_pcf = new PrintWriter(sv_pcf);
		pw_pcf.write(rep);
		pw_pcf.flush();
		pw_pcf.close();
		File pcf = new File("trunk/xml/sv_pcf.xml");
		parseur.parse(pcf, gestionnaire);
	}

	/**
	 * Fermeture de la connexion
	 * 
	 * @param in
	 *            flux d'entree
	 * @param out
	 *            flux de sortie
	 * @throws IOException
	 *             exception
	 */
	private static void close() throws IOException {
		// fermeture
		System.out.println("Fermeture connexion.");
		out.print(PCF.getPCF("pcf" + getReqId(), PcfType.request,
				PCF.getByeXML()));
		out.flush();
		socket.close();
	}
}
