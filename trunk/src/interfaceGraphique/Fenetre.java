package interfaceGraphique;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import socket.GestionSocketView;
import socket.InitException;

/**
 * La fenetre graphique
 * 
 * @author Kevcoq
 * 
 */
public class Fenetre {
	// attribut
	private JPanel jp;
	private JFrame jFra;

	/**
	 * Constructeur
	 */
	public Fenetre() {
		jFra = new JFrame("Controleur ferroviaire");

		jp = new JPanel();
		jp.setPreferredSize(new Dimension(600, 400));
		jp.setLayout(new BorderLayout());

		jFra.setContentPane(jp);
		jFra.setVisible(true);
		jFra.pack();
		jFra.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				try {
					GestionSocketView.get().close();
					jFra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InitException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * ajoute le componant au panel
	 * 
	 * @param obj
	 *            un composant graphique
	 */
	public void addJP(Component obj) {
		jp.add(obj);
	}

	/**
	 * obtient la largeur du panel
	 * 
	 * @return la largeur du panel
	 */
	public int getWidth() {
		return jp.getWidth();
	}

	/**
	 * obtient la hauteur du panel
	 * 
	 * @return la hauteur du panel
	 */
	public int getHeight() {
		return jp.getHeight();
	}
}