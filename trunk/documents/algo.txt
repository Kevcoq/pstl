﻿// Pseudo-code de l'algo (un genre de pseudo C pour la coloration)


/**
   Structure de donnée
*/

// un train
struct rame {
  boolean roule;
  time attente;
  capteur *capt;
  void stop();
  void start();
};

// un capteur
struct capteur {
  boolean station;
  boolean actif;
  led *feu;
  capt *suiv;
  capt *prec;
};

// un feu
struct led {
  couleur etat;
};

// un element de la topography
union obj {
  capteur *capt;
  rame *rame;
};

// la topography
struct scenario {
  obj *obj;
  obj *suiv;
  obj *prec;
};







/**
   Fction non défini
*/
/* Phase d'init */
void connexion(void); // connexion au serveur
void lecture_scenario(scenario *s); // lect scenario
void lecture_typo(scenario *s); // lect typo
void placement_train(scenario *s); // lect emplacement des trains
void start(); // lance les trains | !!! si un train est en attente

/* Fctionnement */
// capteur
int* lire_capt_act(void); // renvoie la liste des id des capteurs actif
capteur rech_capt(int id); // renvoie le capteur d'identifiant id
boolean estStation(capteur capt); // vrai si capt est un capteur de gare

// led
led led_suiv(capteur capt); // renvoie la led qui suit capt
led led_prec(capteur capt); // renvoie la led qui precede capt

// rame
rame rech_rame(capteur capt); // renvoie le train ayant atteint le capteur










/**
   Le prog principal
*/
void main {
  connexion(); // connexion au serveur
  initialisation(); // lecture du scenario et de la typo du circuit
  thread_lecture(); // lancement du thread
  start(); // go go go !!!
}




/**
   Phase d'initialisation
*/
void initialisaion {
  lecture_scenario(); // lect scenario
  lecture_typo(); // lect typo
  placement_train(); // lect emplacement des trains
}



/**
   Thread de lecture et d'analyse du xml recu
*/
void thread_lecture {
  rame rame_att[] = []; // liste des rames arreté
  
  while(true) {

    // on recup. la liste des capt. actif
    int ids_act[] = lire_capt_act();


    // pour chaque id
    for (int id : ids_act) {

      // on recup. le capt correspondant a l'id
      capteur capt = rech_capt(id);
      
      
      // on recup la rame qui a declenché le capt
      rame r = capt->train = capt->prec;
      r->capt = capt;

      // on test le type de capt
      if (estStation(capt)){
	// si c'est une gare, on stop le train
	r.stop();
	r.attente = syst_time + random_int(ATT_MAX);
	rame_att.add(r);


	// on recup la led associé au capt
	led feu = capt->feu;
	if(feu != NULL) { // scenar 3, pas de feu station
	  // on met le feu a rouge
	  feu.etat = rouge;
	}
      } 


      else {
	// on recup la led associé au capt
	led feu = capt->feu;
	// on met le feu a rouge
	feu.etat = rouge;

	if (led_suiv(capt).etat == rouge) {
	  // si le feu d'apres est rouge, on stop le train
	  r.stop();
	  rame_att.add(r);
	} 

	else {
	  // sinon le train poursuit et le feu prec passe au vert
	  led_prec(capt).etat = vert;
	}
      }	
    }      
     

    // on check les trains en attente
    for (rame r : rame_att) {
      if ((r.attente == NULL) || (syst_time >= r.attente)) {
	// si le tps d'attente est null ou bien fini

	capteur capt = r->capt;

	if(estStation(capt) && capt->feu == NULL) {
	  // feu trancon + arret gare, on repart
	  r.start();
	}

	else {
	  if (led_suiv(capt).etat == vert) {
	    // si le feu suivant est vert
	    r.start();
	    led_prec(capt).etat = vert;
	    rame_att.remove(r);
	  }
	}
      }
    }
  }
}
